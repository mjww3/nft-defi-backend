import {MigrationInterface, QueryRunner} from "typeorm";

export class addNonceToUser1624937554628 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER table "users" add current_nonce varchar(100)`)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
