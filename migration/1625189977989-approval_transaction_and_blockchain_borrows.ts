import {MigrationInterface, QueryRunner} from "typeorm";

export class approvalTransactionAndBlockchainBorrows1625189977989 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await Promise.all([queryRunner.query(`ALTER table "borrows" add approval_transaction_id varchar(300)`),queryRunner.query(`ALTER table "borrows" add blockchain varchar(100)`)])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
