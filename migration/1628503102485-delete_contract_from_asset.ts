import {MigrationInterface, QueryRunner} from "typeorm";

export class deleteContractFromAsset1628503102485 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        queryRunner.query(`ALTER table "assets" drop column contract cascade`)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
