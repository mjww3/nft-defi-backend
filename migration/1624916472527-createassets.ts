import {MigrationInterface, QueryRunner} from "typeorm";

export class createassets1624916472527 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "assets" ( "id" serial not null, "name" varchar(100), "description" varchar(100), "metadata" jsonb , "traits" jsonb , contract_address varchar(100) not null, asset_id_blockchain varchar(100), is_published bool, owner varchar(100) not null, is_active bool, animation_url varchar(1000), image_url varchar(1000)  ,constraint "PK_c0911b1d44db6cdd30d6afc9" PRIMARY KEY ("id") )`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "assets"`);
    }

}
