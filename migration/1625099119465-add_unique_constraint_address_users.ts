import {MigrationInterface, QueryRunner} from "typeorm";

export class addUniqueConstraintAddressUsers1625099119465 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER table "users" add unique (address)`)

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
