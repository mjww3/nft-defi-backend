import {MigrationInterface, QueryRunner} from "typeorm";

export class addAssetContractTable1625193313824 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "assetcontracts" ( "id" serial not null, "asset_id" integer not null, "contract_address" varchar(100) not null, "asset_contract_type" varchar(100) not null, "contract_type" varchar(100) not null, "permalink" varchar(100) , "total_supply" varchar(100), constraint "PK_c0911b1dff44db6cdd303c6dd6afc9" PRIMARY KEY ("id"),  constraint fk_asset_contract foreign key(asset_id) references assets(id))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
