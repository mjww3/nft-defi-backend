import {MigrationInterface, QueryRunner} from "typeorm";

export class addContractToAsset1625198150785 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        Promise.all([
            queryRunner.query(`ALTER table "assets" add contract integer`),
        ])

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
