import { query } from "express";
import {MigrationInterface, QueryRunner} from "typeorm";

export class foreignKeyAssetsBorrow1628613648603 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "borrows" ( "id" serial not null, "address" varchar(100), "amount" bigint not null , "loan_to_value_ratio" bigint ,"risk_factor" bigint ,"asset_id" integer ,"comments" varchar(1000), "owner" varchar(100) , "borrow_id" integer, constraint "PK_c0911b1d44db6cdd303c6dd6afc9" PRIMARY KEY ("id"), constraint fk_asset foreign key(asset_id) references assets(id))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
