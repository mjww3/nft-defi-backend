import {MigrationInterface, QueryRunner} from "typeorm";

export class addCurrencyAssets1625003977233 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER table "assets" add currency varchar(100)`)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
