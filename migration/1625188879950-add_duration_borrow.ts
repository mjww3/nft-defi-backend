import {MigrationInterface, QueryRunner} from "typeorm";

export class addDurationBorrow1625188879950 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER table "borrows" add duration integer`)

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
