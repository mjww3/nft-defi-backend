import {MigrationInterface, QueryRunner} from "typeorm";

export class addForeignKeyUsersToAssets1625106284535 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await Promise.all([  
         queryRunner.query(`ALTER table "borrows" drop column owner`),
         queryRunner.query(`ALTER table "borrows" add owner integer`),
         queryRunner.query(`ALTER table "borrows" add constraint fk_user_owner foreign key (owner) references users(id)`),
         queryRunner.query(`ALTER table "borrows" drop column address`)])

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
