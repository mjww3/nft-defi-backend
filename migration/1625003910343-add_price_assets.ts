import {MigrationInterface, QueryRunner} from "typeorm";

export class addPriceAssets1625003910343 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER table "assets" add price varchar(100)`)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
