import {MigrationInterface, QueryRunner} from "typeorm";

export class addContractTypeAssets1625001078798 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER table "assets" add contract_type varchar(100)`)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
