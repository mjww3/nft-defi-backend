import {MigrationInterface, QueryRunner} from "typeorm";

export class addPlatformNameAssets1625001614904 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER table "assets" add platform_name varchar(100)`)

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
