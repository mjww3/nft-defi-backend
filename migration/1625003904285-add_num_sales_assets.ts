import {MigrationInterface, QueryRunner} from "typeorm";

export class addNumSalesAssets1625003904285 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER table "assets" add num_sales bigint`)

    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
