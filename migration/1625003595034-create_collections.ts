import {MigrationInterface, QueryRunner} from "typeorm";

export class createCollections1625003595034 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "collections" ( "id" serial not null, "name" varchar(100), "description" varchar(100), "owner" varchar(100), "is_active" bool, "image_url" varchar(1000) ,constraint "PK_c0911b1d44db6cdd3s0d6afc9" PRIMARY KEY ("id") )`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
