import {MigrationInterface, QueryRunner} from "typeorm";

export class createusers1624916467033 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "users" ( "id" serial not null, "name" varchar(100), "address" varchar(100) not null, "blockchain" varchar(100) not null, "profile_pic" varchar(200) , constraint "PK_c0911b1d44db6cdd303c6d6afc9" PRIMARY KEY ("id") )`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "users"`);
    }

}
