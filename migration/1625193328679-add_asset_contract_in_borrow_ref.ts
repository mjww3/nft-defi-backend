import {MigrationInterface, QueryRunner} from "typeorm";

export class addAssetContractInBorrowRef1625193328679 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
       await Promise.all([queryRunner.query(`ALTER table "borrows" add contract integer`),queryRunner.query(`ALTER table "borrows" add constraint fk_asset_contract foreign key (contract) references assetcontracts(id)`)])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }

}
