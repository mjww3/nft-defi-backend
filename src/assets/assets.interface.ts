import Collection from '../collections/collections.entities';
import { Users } from '../users/users.entities';

//asset interface
export interface Assets {
  id: string | number;
  platform_name: string;
  name?: string;
  asset_id_blockchain: string;
  owner: Users;
  description: string;
  image_original_url?: string;
  collection?: Collection;
  num_sales: number;
  animation_original_url?: string;
  price: string;
  contract_address?: string;
  contract_type?: string;
  currency?: string;
}

//asset contract
export interface AssetContract {
  contract_address: string;
  contract_type: string; //erc721 or erc1155
  contract_name?: string; //name of the contract
  description?: string; //description of the contract
  asset_contract_type: string;
  asset_contract_created?: string; // will be having the datetime when this asset contract is created
}
