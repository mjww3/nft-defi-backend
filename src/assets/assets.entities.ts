/* eslint-disable @typescript-eslint/ban-types */
import { AssetContract, Borrow } from '../borrow/borrows.entities';
import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  OneToOne,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { Users } from '../users/users.entities';
import Transaction from '../transactions/transactions.entities';
import Collection from '../collections/collections.entities';
import { IsOptional } from 'class-validator';

@Entity('assets')
export class Asset {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public name: string;

  @Column({ nullable: false, default: 'ACTIVE' })
  public status: string;

  @Column()
  public description: string;

  @Column({ type: 'jsonb', nullable: true })
  public metadata: {};

  @Column({ type: 'jsonb', nullable: true })
  public traits: {};

  @Column()
  public currency: string;

  @Column()
  public contract_address: string;

  @Column()
  public asset_id_blockchain: string;

  @Column({ default: true })
  public is_published: boolean;

  @ManyToOne(() => Users, (users) => users.id)
  @JoinColumn()
  owner: Users;

  @OneToMany(() => Transaction, (transaction) => transaction.asset)
  transactions: Transaction[];

  @Column({ default: true })
  public is_active: boolean;

  @Column({ nullable: true })
  public animation_url: string;

  @Column({ nullable: true })
  public image_url: string;

  @Column()
  public contract_type: string; //can be erc721 / erc1155

  @Column()
  public platform_name: string;

  @Column()
  public num_sales: number;

  @Column()
  public price: string;

  @Column({ nullable: true })
  public rating: number;

  @Column('text', { nullable: true, array: true })
  public tag: string[];

  @Column({ nullable: true })
  public pool_type: number;

  @OneToOne(() => Borrow, (borrow) => borrow.id)
  @JoinColumn()
  @IsOptional()
  public borrow_asset_id: Borrow;

  @ManyToOne(() => Collection, (collection) => collection.assets)
  @JoinColumn()
  public collection: Collection;
}
