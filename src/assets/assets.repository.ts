/* eslint-disable prefer-const */
import { Asset } from './assets.entities';
import { EntityRepository, Repository } from 'typeorm';
import { CreateAssetDto } from './interfaces/createAsset.dto';
import { GetAssetsDto } from './interfaces/getAssets.dto';
import { GetAssetDto } from './interfaces/getAsset.dto';
import { UpdateAssetDto } from './interfaces/updateAssets.dto';
import { Users } from 'src/users/users.entities';

//assets
@EntityRepository(Asset)
export class AssetRepository extends Repository<Asset> {
  createAsset = async (createassetdto: CreateAssetDto) => {
    const asset = this.create(createassetdto);
    await this.save(asset);
    return asset;
  };

  getAsset = async (getassetdto: GetAssetDto) => {
    const asset = await this.createQueryBuilder('asset')
      .innerJoinAndSelect('asset.borrow_asset_id', 'borrow')
      .where('asset.id  = :asset_id', { asset_id: getassetdto.id })
      .getOne();
    console.log('asset_id', asset);
    return asset;
  };

  getAssets = async (getassetdto: GetAssetsDto) => {
    const { limit, offset } = getassetdto;
    const { sort_key, sort_value } = getassetdto;
    let {
      min_asset_value,
      max_asset_value,
      min_loan_value,
      max_loan_value,
      max_loan_duration,
    } = getassetdto;

    const obj: any = { where: getassetdto };
    if (limit) {
      obj.take = limit;
    }
    if (offset) {
      obj.skip = offset;
    }
    if (sort_key) {
      obj.order = { sort_key: 'ASC' };
    }
    if (sort_value) {
      if (sort_key == 'asc') {
        obj.order.sort_key = 'ASC';
      } else {
        obj.order.sort_key = 'DESC';
      }
    }
    if (parseInt(min_loan_value) < 0) {
      min_loan_value = '0';
    }
    if (parseInt(min_asset_value) < 0) {
      min_asset_value = '0';
    }
    let query = this.createQueryBuilder('asset')
      .innerJoinAndSelect('asset.borrow_asset_id', 'borrow')
      .innerJoinAndSelect('asset.collection', 'collection')
      .where('borrow.duration <= :max_loan_duration ', {
        max_loan_duration: max_loan_duration ? max_loan_duration : 90,
      })
      .andWhere('borrow.amount >= :min_loan_value', {
        min_loan_value: min_loan_value ? min_loan_value : '0',
      })
      // .andWhere('asset.price <= :max_asset_value', {
      //   max_asset_value: max_asset_value ? max_asset_value : '10000000',
      // })
      // .andWhere('asset.price >= :min_asset_value', {
      //   min_asset_value: min_asset_value ? min_asset_value : '0',
      // })
      .andWhere('asset.status = :status', { status: 'ACTIVE' })
      .limit(<number>(<unknown>limit))
      .offset(<number>(<unknown>offset));
    if (getassetdto.tag) {
      query = query.andWhere('asset.tag = :tag', { tag: getassetdto.tag });
    }
    if (getassetdto.max_loan_value) {
      query = query.andWhere('borrow.amount <= :max_loan_value', {
        max_loan_value: max_loan_value,
      });
    }
    if (getassetdto.collection_id) {
      query = query.andWhere('asset.collectionId = :collection_id', {
        collection_id: getassetdto.collection_id,
      });
    }
    if (getassetdto.search_string) {
      query = query
        .andWhere('to_tsvector(asset.name) @@ to_tsquery(:query_string)', {
          query_string: getassetdto.search_string,
        })
        .orWhere(
          'to_tsvector(asset.description) @@ to_tsquery(:query_string)',
          { query_string: getassetdto.search_string },
        );
    }
    const [assets, count] = await query.getManyAndCount();
    return [assets, count];
  };

  getAssetByOwner = async (user: Users) => {
    const assets = await this.find({ owner: user });
    return assets;
  };

  updateAsset = async (id: string, updateassetdto: UpdateAssetDto) => {
    await this.update(id, updateassetdto).then((response) => response.raw[0]);
    const asset = await this.findOne(id);
    return asset;
  };
}
