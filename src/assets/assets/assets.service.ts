import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { Assets } from '../assets.interface';
import { AssetRepository } from '../assets.repository';
import { GetAssetsDto } from '../interfaces/getAssets.dto';
import { UpdateAssetDto } from '../interfaces/updateAssets.dto';
import { GetAssetDto } from '../interfaces/getAsset.dto';
import { Users } from 'src/users/users.entities';

@Injectable()
export class AssetsService {
  //get asset
  private assetRepository: AssetRepository;
  constructor(private readonly connection: Connection) {
    this.assetRepository = this.connection.getCustomRepository(AssetRepository);
  }

  //get asset
  async getAsset(getassetdto: GetAssetDto): Promise<Assets> {
    return await this.assetRepository.getAsset(getassetdto);
  }

  //get assets
  async getAssets(getassetdto: GetAssetsDto): Promise<any> {
    return await this.assetRepository.getAssets(getassetdto);
  }

  async getAssetByOwner(user: Users): Promise<any> {
    return await this.assetRepository.getAssetByOwner(user);
  }

  ///update asset
  async updateAsset(
    id: string,
    updateassetdto: UpdateAssetDto,
  ): Promise<Assets> {
    return await this.assetRepository.updateAsset(id, updateassetdto);
  }
}
