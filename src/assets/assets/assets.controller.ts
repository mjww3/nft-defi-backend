import {
  ApiBadRequestResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import {
  Controller,
  Get,
  InternalServerErrorException,
  Query,
  UnauthorizedException,
  UsePipes,
  Headers,
  ValidationPipe,
} from '@nestjs/common';
import { GetAssetsDto } from '../interfaces/getAssets.dto';
import { AssetsService } from './assets.service';
import { ApiBadGatewayResponse, ApiResponse, ApiTags } from '@nestjs/swagger';
import { GetAssetDto } from '../interfaces/getAsset.dto';
import jwtHelper from '../../helpers/jwt';

@ApiTags('Assets')
@Controller()
export class AssetsController {
  constructor(private readonly assetService: AssetsService) {}

  //get an asset
  @ApiResponse({ status: 200 })
  @Get('/asset')
  @UsePipes(new ValidationPipe())
  async getAsset(@Query() getassetdto: GetAssetDto) {
    const asset = await this.assetService.getAsset(getassetdto);
    return { data: asset };
  }

  //get multiple assets
  //supports filter query too
  @ApiResponse({ status: 200 })
  @Get('/assets')
  @UsePipes(new ValidationPipe())
  async getAssets(@Query() getassetsdto: GetAssetsDto) {
    const assets = await this.assetService.getAssets(getassetsdto);
    return { data: assets[0], count: assets[1] };
  }

  @ApiResponse({ status: 200 })
  @ApiBadRequestResponse({ status: 400 })
  @ApiUnauthorizedResponse({ status: 401 })
  @ApiBadGatewayResponse({ status: 401 })
  @Get('/getAssetByOwner')
  @UsePipes(new ValidationPipe())
  async GetFinalizedLoansByUser(@Headers() headers) {
    const authorization = headers['authorization'];
    if (!authorization) {
      return new UnauthorizedException('unauthorized');
    }
    //now get the user id after decoding the jwt
    const data = await jwtHelper.verifyJWT(authorization);
    if (data.error && !data.data) {
      return new UnauthorizedException('unauthorized');
    } else {
      const decoded = data.data;
      if (!decoded.data && !decoded.data.id) {
        return new InternalServerErrorException('Internal server error');
      }
      const assets = await this.assetService.getAssetByOwner(decoded.data.id);
      return { data: assets };
    }
  }
}
