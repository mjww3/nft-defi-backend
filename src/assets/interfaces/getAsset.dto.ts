import { ApiPropertyOptional,ApiProperty } from '@nestjs/swagger';
import {IsString, IsNumberString, IsNumber, IsDefined, IsOptional, IsBoolean} from 'class-validator';
import { Users } from '../../users/users.entities';

export class GetAssetDto {
    @ApiProperty()
    @IsString()
    id: string;
}