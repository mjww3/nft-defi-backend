import { ApiProperty } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty, IsNumberString} from 'class-validator';
import Collection from '../../collections/collections.entities';
import { Users } from '../../users/users.entities';

export class UpdateAssetDto{
    @IsString()
    @ApiProperty()
    origin: string;

    @IsString()
    @ApiProperty()
    owner: Users;

    @IsString()
    @ApiProperty()
    collection: Collection;

    @IsNumberString()
    @ApiProperty()
    num_sales: number;

    @IsString()
    animation_original_url: string;

    @IsString()
    price: string;
}
