import {IsString, IsNumberString, IsDefined, IsObject, IsBoolean, IsOptional} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Users } from '../../users/users.entities';

export class CreateAssetDto{
    @IsString()
    @IsDefined()
    @ApiProperty()
    name: string;

    @IsString()
    @ApiProperty()
    @IsOptional()
    description?: string;

    @IsObject()
    @ApiProperty()
    @IsOptional()
    metadata?: object;

    @IsObject()
    @ApiProperty()
    @IsOptional()
    traits?: object;

    @IsObject()
    @ApiProperty()
    @IsOptional()
    pool_type?: number;

    @IsString()
    @IsDefined()
    @ApiProperty()
    contract_address: string;

    @IsString()
    @IsDefined()
    @ApiProperty()
    asset_id_blockchain: string;

    @IsBoolean()
    @IsDefined()
    is_published: boolean;

    @IsString()
    @IsDefined()
    owner: Users;

    @IsBoolean()
    @IsDefined()
    is_active: boolean; //acts as the soft delete flag for the assets

    @IsString()
    @ApiProperty()
    @IsOptional()
    animation_url: string;

    @IsString()
    @IsDefined()
    @ApiProperty()
    image_url: string;

    @IsString()
    @ApiProperty()
    @IsDefined()
    contract_type: string; //can be erc721/erc1155
}
