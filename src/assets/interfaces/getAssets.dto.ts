import { ApiPropertyOptional,ApiProperty } from '@nestjs/swagger';
import {IsString, IsNumberString, IsNumber, IsDefined, IsOptional, IsBoolean} from 'class-validator';
import { Users } from '../../users/users.entities';

export class GetAssetsDto {
    @ApiPropertyOptional()
    owner: Users;

    @IsString()
    @ApiProperty()
    limit?: string;

    @IsString()
    @ApiProperty()
    offset?: string;

    @ApiPropertyOptional()
    currency: string;

    @ApiPropertyOptional()
    contract_address: string;

    @ApiPropertyOptional()
    is_published: boolean;

    @ApiPropertyOptional()
    contract_type: string;

    @ApiPropertyOptional()
    search_string: string;

    @ApiPropertyOptional()
    min_asset_value: string;

    @ApiPropertyOptional()
    tag: string;

    @ApiPropertyOptional()
    collection_id: string;

    @ApiPropertyOptional()
    max_asset_value: string;

    @ApiPropertyOptional()
    min_loan_value: string;

    @ApiPropertyOptional()
    max_loan_value: string;

    @ApiPropertyOptional()
    max_loan_duration: string;

    @ApiPropertyOptional()
    platform_name: string;

    @ApiPropertyOptional()
    sort_key: string;

    @ApiPropertyOptional()
    sort_value: string; //can be asc or desc

    @ApiPropertyOptional()
    pool_type: number;
}