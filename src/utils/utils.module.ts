import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BorrowRepository } from '../borrow/borrow.repository';
import { AssetRepository } from '../assets/assets.repository';
import Transaction from '../transactions/transactions.entities';
import { TransactionRepository } from '../transactions/transactions.repository';
import { UtilsService } from './utils.service';
import { BorrowService } from '../borrow/borrow/borrows.service';
import { UserRepository } from '../users/users.repository';
import { CollectionRepository } from '../collections/collections.repository';

@Module({
  imports: [ConfigModule.forRoot(), TypeOrmModule.forFeature([TransactionRepository,AssetRepository,UserRepository,BorrowRepository, CollectionRepository])],
  providers: [UtilsService]
})
export class UtilsModule {}
