import * as redis from 'redis';
import * as Queue from 'bull';
import IResponse from './interfaces/functionResponse';

class RedisClient {
  client: any;
  transactionQueue: any;

  //redis constructor definition
  constructor() {
    this.client = redis.createClient({
      host: process.env.REDIS_URL,
    });
    this.transactionQueue = new Queue(
      'transaction_create_loan',
      process.env.REDIS_URL || 'redis://127.0.0.1:6379',
    );
  }

  //get from redis
  async get(key: string): Promise<IResponse> {
    return new Promise((resolve, reject) => {
      try {
        this.client.get(key, function (err, response) {
          if (err) {
            return reject({ data: null, error: err });
          } else {
            return resolve({ data: response, error: null });
          }
        });
      } catch (error) {
        return reject({ data: null, error: error });
      }
    });
  }

  //set in redis
  async set(key: string, value: string): Promise<IResponse> {
    return new Promise((resolve, reject) => {
      try {
        this.client.set(key, value, function (err, response) {
          if (err) {
            return reject({ data: null, error: err });
          } else {
            return resolve({ data: response, error: null });
          }
        });
      } catch (error) {
        return reject({ data: null, error: error });
      }
    });
  }

  //function to mget from redis
  async mget(keys: Array<string>): Promise<IResponse> {
    try {
      this.client.mget(keys, function (err, response) {
        if (err) {
          return { data: null, error: err };
        } else {
          return { data: response, error: null };
        }
      });
    } catch (error) {
      return { data: null, error: error };
    }
  }

  //function to mset into redis
  async mset(keys: Array<string>, values: Array<string>): Promise<IResponse> {
    try {
      this.client.mset(keys, values, function (err, response) {
        if (err) {
          return { data: null, error: err };
        } else {
          return { data: response, error: null };
        }
      });
    } catch (error) {
      return { data: null, error: error };
    }
  }

  //function to hget from redis
  async hget(hash: string, key: string): Promise<IResponse> {
    try {
      this.client.hget(hash, key, function (err, response) {
        if (err) {
          return { data: null, error: err };
        } else {
          return { data: response, error: null };
        }
      });
    } catch (error) {
      return { data: null, error: error };
    }
  }

  //function to hset from redis
  async hset(hash: string, key: string, value: string): Promise<IResponse> {
    try {
      this.client.mset(hash, key, value, function (err, response) {
        if (err) {
          return { data: null, error: err };
        } else {
          return { data: response, error: null };
        }
      });
    } catch (error) {
      return { data: null, error: error };
    }
  }

  //function to incr from redis
  async incr(key: string): Promise<IResponse> {
    try {
      this.client.incr(key, function (err, response) {
        if (err) {
          return { data: null, error: err };
        } else {
          return { data: response, error: null };
        }
      });
    } catch (error) {
      return { data: null, error: error };
    }
  }

  //function to decr from redis
  async decr(key: string): Promise<IResponse> {
    try {
      this.client.decr(key, function (err, response) {
        if (err) {
          return { data: null, error: err };
        } else {
          return { data: response, error: null };
        }
      });
    } catch (error) {
      return { data: null, error: error };
    }
  }
}

const redisClient = new RedisClient();
export default redisClient;
