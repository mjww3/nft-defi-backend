/**
 *Description of the function response interface
 */

export interface IFuncResponse{
    error: any ;
    data: any;
}

export default IFuncResponse;