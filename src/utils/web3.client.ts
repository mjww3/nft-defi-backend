const Web3 = require( 'web3');

class Webthree{
    web3: any;

    constructor(){
        this.web3 = new Web3(Web3.givenProvider ||"wss://rinkeby.infura.io/ws/v3/a9e4aa3de809420f8d355b764323069b");
    }
}

const web3 = new Webthree()
export default web3;