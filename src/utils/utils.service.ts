/* eslint-disable @typescript-eslint/no-var-requires */
import IResponse from './interfaces/functionResponse';
import redisClient from './redisClient';
import {
  WEB3_SIGNATURE_NONCE_REDIS_KEY,
  INVALID_SIGNATURE,
} from '../helpers/constants';
import jwthelper from '../helpers/jwt';
import { BorrowRepository } from '../borrow/borrow.repository';
import { TransactionRepository } from '../transactions/transactions.repository';
import { AssetRepository } from '../assets/assets.repository';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
// import { Asset } from '../assets/assets.entities';
// import { Borrow } from '../borrow/borrows.entities';
import { UserRepository } from '../users/users.repository';
import { CollectionRepository } from '../collections/collections.repository';
import { UtilsWorker } from './utils.worker';

const abiLendingData = require('../abi/lending_data.json');
const Web3 = require('web3');

const contracts = [
  {
    name: 'LendingData',
    address: '0xF6646bCC8ae654bcC84F4B272084509727AFa9c9',
    abi: abiLendingData.abi,
  },
];
@Injectable()
export class UtilsService {
  private transactionRepository: TransactionRepository;
  private assetRepository: AssetRepository;
  private userRepository: UserRepository;
  private borrowRepository: BorrowRepository;
  private collectionRepository: CollectionRepository;
  private web3: any;
  private proxyContract: any;
  private tokenProxyContract: any;
  constructor(
    private readonly connection: Connection,
    @InjectRepository(TransactionRepository)
    transactionRepository: TransactionRepository,
    @InjectRepository(AssetRepository) assetRepository: AssetRepository,
    @InjectRepository(UserRepository) userRepository: UserRepository,
    @InjectRepository(BorrowRepository) borrowRepository: BorrowRepository,
    @InjectRepository(CollectionRepository)
    collectionRepository: CollectionRepository,
  ) {
    this.transactionRepository = transactionRepository;
    this.assetRepository = assetRepository;
    this.borrowRepository = borrowRepository;
    this.userRepository = userRepository;
    this.collectionRepository = collectionRepository;
    this.web3 = new Web3(
      Web3.givenProvider ||
        'wss://rinkeby.infura.io/ws/v3/a9e4aa3de809420f8d355b764323069b',
    );
    const utilworker = new UtilsWorker(
      connection,
      transactionRepository,
      assetRepository,
      userRepository,
      borrowRepository,
      collectionRepository,
    );
    this.init();
    utilworker.runWorkerTransactions();
  }

  async init() {
    let last_processed = 0;
    const lastProcessed = await redisClient.get('LAST_PROCESSED');
    console.log('lastProcessed', lastProcessed);
    if (!lastProcessed.data || lastProcessed.error) {
      last_processed = 0;
    } else {
      last_processed = parseInt(lastProcessed.data);
    }
    const options = {
      fromBlock: last_processed,
    };
    const lendingDataContract = new this.web3.eth.Contract(
      abiLendingData.abi,
      contracts[0].address,
    );

    lendingDataContract.events.NewLoan(options).on('data', async (event) => {
      console.log('event.transactionHash', event.transactionHash);
      const hash = event.transactionHash;
      const data = {};
      //check before processing has this been processed already
      redisClient.transactionQueue.add({ event: event });
    });
  }

  //function to check the validity of the address
  public async get_base_info(address: string): Promise<IResponse> {
    return { data: this.web3.utils.isAddress(address), error: false };
  }

  //function to return the nonce to the backend for the signature for a user
  //by default we will start the nonce from 1
  async get_nonce_for_signature(address: string): Promise<IResponse> {
    try {
      const current_nonce = await redisClient.get(
        WEB3_SIGNATURE_NONCE_REDIS_KEY + address,
      );
      if (!current_nonce.error) {
        await redisClient.incr(WEB3_SIGNATURE_NONCE_REDIS_KEY + address);
        return { data: current_nonce.data || 1, error: null };
      } else {
        return { data: null, error: current_nonce.error };
      }
    } catch (error) {
      return { data: null, error: error };
    }
  }

  validate_txhash(addr) {
    return /^0x([A-Fa-f0-9]{64})$/.test(addr);
  }

  //function to get the details of the asset from the contract
  async get_asset_details(address: string, tokenId: string): Promise<any> {
    try {
    } catch (error) {
      return { data: null, error: error };
    }
  }

  //function to check the nonce passed in the api request for a yser
  async check_nonce_for_signature(
    address: string,
    nonce: any,
  ): Promise<IResponse> {
    try {
      const current_nonce = await redisClient.get(
        WEB3_SIGNATURE_NONCE_REDIS_KEY + address,
      );
      if (!current_nonce.error) {
        const value = current_nonce.data || 1;
        return {
          data: parseInt(value) == parseInt(nonce) ? true : false,
          error: null,
        };
      } else {
        return { data: null, error: current_nonce.error };
      }
    } catch (error) {
      return { data: null, error: error };
    }
  }

  //function to verify the signature and the text passed in the signature
  async verify_signature(
    address: string,
    signature: string,
    message: string,
    nonce: any,
    blockchain: string,
  ): Promise<IResponse> {
    try {
      const hash = this.web3.utils.sha3(message);
      const signing_address = await this.web3.eth.personal.ecRecover(
        hash,
        signature,
      );
      if (signing_address.toLowerCase() == address.toLowerCase()) {
        const jwtToken = await jwthelper.createJWT(address, blockchain);
        if (!jwtToken.data || jwtToken.error)
          return { data: null, error: INVALID_SIGNATURE };
        return { data: jwtToken.data, error: false };
      } else {
        return { data: null, error: INVALID_SIGNATURE };
      }
    } catch (error) {
      return { data: null, error: error };
    }
  }

  //function to check if the proxy exists for a address or not
  async check_proxy(address: string): Promise<any> {
    try {
      const has_proxy = await this.proxyContract.methods
        .contracts(address)
        .call();
      return { data: has_proxy, error: null };
    } catch (error) {
      return { data: null, error: error };
    }
  }

  //function to create a proxy of an address
  //this function is mainly used for testing purposes
  async create_proxy(address: string): Promise<any> {
    try {
      const proxy = await this.proxyContract.methods
        .registerProxy()
        .send({ from: process.env.ADMIN_ADDRESS });
      return { data: proxy, error: null };
    } catch (error) {
      return { data: null, error: error };
    }
  }
}
