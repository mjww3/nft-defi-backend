/* eslint-disable @typescript-eslint/no-var-requires */
const Web3 = require('web3');
// import IResponse from './interfaces/functionResponse';
import redisClient from './redisClient';
// import {
//   WEB3_SIGNATURE_NONCE_REDIS_KEY,
//   INVALID_SIGNATURE,
// } from '../helpers/constants';
// import jwthelper from '../helpers/jwt';
// import { CreateBorrowDto } from '../borrow/interfaces/createBorrow.dto';
// import { CreateAssetDto } from '../assets/interfaces/createAsset.dto';
import { BorrowRepository } from '../borrow/borrow.repository';
import { TransactionRepository } from '../transactions/transactions.repository';
import { AssetRepository } from '../assets/assets.repository';
// import { Inject, Injectable } from '@nestjs/common';
// import { InjectRepository } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
// import { Asset } from '../assets/assets.entities';
// import { Borrow } from '../borrow/borrows.entities';
import { UserRepository } from '../users/users.repository';
import { CollectionRepository } from '../collections/collections.repository';
import { getConnection } from 'typeorm';

const abiDecoder = require('abi-decoder');
const abiLendingData = require('../abi/lending_data.json');
abiDecoder.addABI(abiLendingData.abi);
// const Redlock = require('redlock');

// const redlock = new Redlock([redisclient]);
export class UtilsWorker {
  private transactionRepository: TransactionRepository;
  private assetRepository: AssetRepository;
  private userRepository: UserRepository;
  private borrowRepository: BorrowRepository;
  private collectionRepository: CollectionRepository;
  private web3: any;
  private proxyContract: any;
  private tokenProxyContract: any;
  constructor(
    private readonly connection: Connection,
    transactionRepository: TransactionRepository,
    assetRepository: AssetRepository,
    userRepository: UserRepository,
    borrowRepository: BorrowRepository,
    collectionRepository: CollectionRepository,
  ) {
    this.transactionRepository = transactionRepository;
    this.assetRepository = assetRepository;
    this.borrowRepository = borrowRepository;
    this.userRepository = userRepository;
    this.collectionRepository = collectionRepository;
    this.web3 = new Web3(
      Web3.givenProvider ||
        'wss://rinkeby.infura.io/ws/v3/a9e4aa3de809420f8d355b764323069b',
    );
  }

  //function to get the transaction details
  public async get_transactions(hash: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.web3.eth.getTransaction(hash, function (error, response) {
        if (error) {
          return reject(error);
        } else {
          return resolve(response);
        }
      });
    });
  }

  //function to get the decoded tx details
  public async decode_tx_hash(hash: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.web3.eth.getTransaction(hash, function (error, response) {
        if (error) {
          return reject(error);
        } else {
          const decodedData = abiDecoder.decodeMethod(response.input);
          return resolve(decodedData);
        }
      });
    });
  }

  //worker function
  async runWorkerTransactions() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this;
    redisClient.transactionQueue.process(async function (job, done) {
      const event = job.data.event;
      const hash = event.transactionHash;
      const transaction = await self.transactionRepository.findOne({
        tx_hash: hash.toLowerCase(),
        status: 'MINED',
      });
      if (!transaction) {
        await getConnection().transaction(async () => {
          const tx = self.transactionRepository.create({
            tx_hash: hash.toLowerCase(),
            status: 'MINED',
            tx_type: 'CREATE_LOAN',
          });
          await self.transactionRepository.save(tx);
          //get the details of the transaction from hash
          const response = await self.get_transactions(hash);
          console.log('response', response);
          const decodedData = abiDecoder.decodeMethod(response.input);
          console.log('decodedData', decodedData);
          if (decodedData.name == 'createLoan') {
            const params: any = {};
            if (decodedData.params.length > 0) {
              decodedData.params.map((param) => {
                if (param.name == 'nftTokenTypeArray') {
                  if (param.value[0] == '1') {
                    param.value = 'ERC1155';
                  } else if (param.value[0] == '0') {
                    param.value = 'ERC721';
                  }
                }
                if (param.name == 'currency') {
                  if (
                    param.value == '0x0000000000000000000000000000000000000000'
                  ) {
                    param.value = 'ETHEREUM';
                  }
                }
                params[param.name] = param.value;
              });

              const userAddress = response.from.toLowerCase();
              //check if userAddress in database
              try {
                const user = await self.userRepository.findOne({
                  address: userAddress,
                });
                if (user) {
                  //user is found now fetch asset details from opensea / rarible
                  const asset_opensea: any =
                    await self.borrowRepository.getSingleAssetOpensea(
                      params.nftAddressArray[0],
                      params.nftTokenIdArray[0],
                      user,
                    );
                  const asset_rarible: any =
                    await self.borrowRepository.getSingleAssetRarible(
                      params.nftAddressArray[0],
                      params.nftTokenIdArray[0],
                      user,
                    );
                  let asset;
                  let borrow;
                  try {
                    borrow = self.borrowRepository.create({
                      amount: params.loanAmount,
                      approval_transaction_id: hash,
                      blockchain: 'ETHEREUM',
                      comments: '',
                      duration: params.nrOfInstallments,
                      status: 'ADDED',
                      risk_factor: 0,
                    });
                    await self.borrowRepository.save(borrow);
                  } catch (error) {
                    console.log('error', error);
                    done();
                  }
                  if (Object.keys(asset_opensea).length > 0) {
                    const collection_name = asset_opensea.collection;
                    const collection =
                      await self.collectionRepository.createCollection({
                        name: collection_name,
                        description: '',
                        image_url: '',
                        is_active: true,
                      });
                    asset = self.assetRepository.create({
                      ...asset_opensea,
                      collection: collection.id,
                      currency: params.currency,
                      contract_address: params.nftAddressArray[0].toLowerCase(),
                      contract_type: params.nftTokenTypeArray,
                      image_url: asset_opensea.image_original_url,
                      borrow_asset_id: borrow.id,
                      // price: params.assetsValue,
                    });
                    await self.assetRepository.save(asset);
                  } else if (Object.keys(asset_rarible).length > 0) {
                    const collection = asset_rarible.collection;
                    asset = self.assetRepository.create({
                      ...asset_rarible,
                      collection: asset_rarible.id,
                      currency: params.currency,
                      contract_address: params.nftAddressArray[0].toLowerCase(),
                      contract_type: params.nftTokenTypeArray,
                      image_url: asset_rarible.image_original_url,
                      borrow_asset_id: borrow.id,
                      // price: params.assetsValue,
                    });
                    await self.collectionRepository.createCollection({
                      name: collection,
                      description: '',
                      image_url: '',
                      is_active: true,
                    });
                    asset = self.assetRepository.create(asset_rarible);
                    await self.assetRepository.save(asset);
                  }
                  console.log('saved for the transactions');
                  done();
                }
              } catch (error) {
                console.log(error);
                done();
              }
            } else {
              done();
            }
          } else {
            done();
          }
        });
      } else {
        done();
      }
    });
  }
}
