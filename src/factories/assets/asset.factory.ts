const Faker = require('faker');
import {Asset} from '../../assets/assets.entities';
import {AssetContract, Borrow} from '../../borrow/borrows.entities'
import { define } from 'typeorm-seeding';
import { meta } from 'eslint/lib/rules/*';
import { Users } from '../../users/users.entities';
import { UserRepository } from '../../users/users.repository';
import { BorrowRepository } from '../../borrow/borrow.repository';
import { assert } from 'console';
const web3 = require("web3"); 
const Blockchains = ["MATIC","ETHEREUM","BINANCE_CHAIN"];
const ContractTypes = ['ERC1155', 'ERC721'];
const PoolTypes = [1,2];
const owner = new Users();
owner.address = "0x5AEC3b2BCb6544Eb4B25C425A747eFc5Da2F2367";
owner.id = 12;
owner.name = "test2";
import {factory} from 'typeorm-seeding'

define(Asset, ( faker: typeof Faker) => {
    const name = faker.lorem.word();
    const description = faker.lorem.word();
    const metadata = {"key":faker.random.objectElement()};
    const traits = {"trait":faker.random.objectElement()}
    const contract_address = "0xa097ee76925e0e559272b6f7a531f5ecce88c655";
    const asset_id_blockchain = faker.random.number();
    const is_published = true;
    const is_active = true;
    const animation_url = faker.image.imageUrl();
    const image_url = faker.image.imageUrl();
    const contract_type = 'erc1155';
    const platform_name = "opensea";
    const num_sales = faker.random.number();
    const price = faker.random.number();

    const asset  = new Asset();
    asset.currency = Blockchains[Math.floor(Math.random() * 3)];
    const pool_type = PoolTypes[Math.floor(Math.random() * 2)];

    asset.name = name;
    asset.description = description;
    asset.metadata = metadata;
    asset.traits = traits;
    asset.contract_address = contract_address;
    asset.asset_id_blockchain = asset_id_blockchain;
    asset.is_published = is_published;
    asset.is_active = is_active;
    asset.animation_url = animation_url;
    asset.owner = owner;
    asset.image_url = image_url;
    asset.contract_type = contract_type;
    asset.platform_name = platform_name;
    asset.num_sales = num_sales;
    asset.price = price;
    asset.pool_type = pool_type;
    
    return asset;
})

define( Borrow, ( faker: typeof Faker, currency:string) =>{
    const borrow = new Borrow();
    borrow.amount = faker.random.number();
    borrow.duration = faker.random.number();
    borrow.asset_id = factory(Asset)() as any;
    borrow.blockchain = currency || "ETHEREUM";
    borrow.risk_factor = Math.floor(Math.random()*100)
    borrow.status = "PENDING";
    borrow.comments = faker.lorem.word();
    borrow.approval_transaction_id = "0x09cb1c5b944cb523da384e38aced674a05f3e03aa5d5f95e5c10d5ae182c4c02";
    console.log( borrow )
    return borrow;
})

// define(AssetContract, (faker: typeof Faker, context:{ asset:Asset}) => {
//     const assetContractFactory = new AssetContract();
//     assetContractFactory.asset_id = context.asset;
//     assetContractFactory.contract_address = "0xa097ee76925e0e559272b6f7a531f5ecce88c655";
//     assetContractFactory.asset_contract_type = 'non-fungible';
//     assetContractFactory.contract_type = ContractTypes[Math.floor(Math.random()*2)]
//     assetContractFactory.blockchain = context.asset.currency;
//     assetContractFactory.permalink = faker.image.imageUrl();
//     return assetContractFactory;
// });