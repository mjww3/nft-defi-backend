const Faker = require('faker');
import { define } from 'typeorm-seeding';
import { meta } from 'eslint/lib/rules/*';
import { Users } from '../../users/users.entities';
import { UserRepository } from '../../users/users.repository';
import Collection from '../../collections/collections.entities';
const web3 = require("web3"); 
const Blockchains = ["MATIC","ETHEREUM","BINANCE_CHAIN"];
const ContractTypes = ['ERC1155', 'ERC721'];
const PoolTypes = [1,2];
const owner = new Users();
owner.address = "0x5AEC3b2BCb6544Eb4B25C425A747eFc5Da2F2367";
owner.id = 12;
owner.name = "test2";

define(Collection, ( faker: typeof Faker) => {
    let collection = new Collection();
    collection.id = faker.random.number();
    collection.description = faker.lorem.text();
    collection.image_url = faker.image.imageUrl();
    collection.name = faker.name.firstName();
    collection.is_active = true;
    return collection;
})