import IResponse from '../utils/interfaces/functionResponse';
const jwt = require('jsonwebtoken');

//JWT helper to create the JWT token after the verification of the signature
class JWTHelper {
  jwtSecret: any;
  constructor() {
    this.jwtSecret = 'defi';
  }

  //function to create a jwt token
  async createJWT(address: string, blockchain: string): Promise<IResponse> {
    return new Promise((resolve, reject) => {
      try {
        jwt.sign(
          this.enhance({ address }, blockchain),
          this.jwtSecret,
          function (error, response) {
            if (error) {
              return resolve({ data: null, error: error });
            } else {
              return resolve({ data: response, error: null });
            }
          },
        );
      } catch (error) {
        return resolve({ data: null, error: error });
      }
    });
  }

  //function to verify a jwt token
  async verifyJWT(token: string): Promise<IResponse> {
    return new Promise((resolve, reject) => {
      try {
        jwt.verify(token, this.jwtSecret, function (error, response) {
          if (error) {
            return resolve({ data: null, error: error });
          } else {
            return resolve({ data: response, error: null });
          }
        });
      } catch (error) {
        return resolve({ data: null, error: error });
      }
    });
  }

  //function to enhance the jwt object to be signed by the server key
  public enhance(obj, blockchain) {
    return {
      ...obj,
      blockchain: blockchain,
      environment: process.env.NODE_ENV || 'testnet',
    };
  }
}

const jwtHelper = new JWTHelper();
export default jwtHelper;
