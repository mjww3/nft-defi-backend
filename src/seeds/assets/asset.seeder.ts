import {Factory, Seeder} from 'typeorm-seeding';
import { Connection } from 'typeorm'
import {Asset} from '../../assets/assets.entities';
import {AssetContract, Borrow} from '../../borrow/borrows.entities'

export class CreateAssets implements Seeder {
    async run(factory: Factory, connection: Connection) {
        const asset:Asset[]  = await factory(Asset)().createMany(2);
        asset.map( async a => {
            await factory(Borrow)(a.currency).create();
        })
   }
  }