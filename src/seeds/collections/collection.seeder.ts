import {Factory, Seeder} from 'typeorm-seeding';
import { Connection } from 'typeorm'
import {AssetContract} from '../../borrow/borrows.entities'
import Collection from '../../collections/collections.entities';

export class CreateCollections implements Seeder {
    async run(factory: Factory, connection: Connection) {
        await factory(Collection)().createMany(20);
   }
  }