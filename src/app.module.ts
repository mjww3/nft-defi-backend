import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { CollectionsModule } from './collections/collections.module';
import { AssetsModule } from './assets/assets.module';
import { LendModule } from './lend/lend.module';
import { BorrowModule } from './borrow/borrow.module';
import { ConfigModule } from '@nestjs/config';
import {TypeOrmModule} from '@nestjs/typeorm';
import * as Joi from '@hapi/joi';
import { Users } from './users/users.entities'
import { Asset } from './assets/assets.entities'
import { AssetContract, Borrow } from './borrow/borrows.entities';
import { PoolModule } from './pool/pool.module';
import { TransactionsModule } from './transactions/transactions.module';
import Collection from './collections/collections.entities';
import Transaction from './transactions/transactions.entities';
import {  NestModule, MiddlewareConsumer} from '@nestjs/common';
import {AuthMiddleware} from './users/users.auth.middleware'
import {RequestMethod} from '@nestjs/common'
import { UsersService } from './users/users/users.service';
import { TransactionsService } from './transactions/transactions.service';
import { UtilsModule } from './utils/utils.module';
import { UtilsService } from './utils/utils.service';
import Lend from './lend/lend.entities'
@Module({
  imports: [  ConfigModule.forRoot(),TypeOrmModule.forRoot({
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    synchronize: true,
    entities:[Users,Asset,Borrow,AssetContract,Collection,Transaction,Lend]
  }),UsersModule, CollectionsModule, AssetsModule, LendModule, BorrowModule, PoolModule, TransactionsModule,
    ],
  controllers: [AppController],
  providers: [AppService,UsersService],
})
export class AppModule implements NestModule{
  configure(userContext: MiddlewareConsumer){
     userContext
        .apply(AuthMiddleware)
        .forRoutes({path: '/user', method: RequestMethod.PUT})
  }
}