import { Asset } from '../assets/assets.entities';
import { Users } from '../users/users.entities';
import {PrimaryGeneratedColumn, Column, Entity, ManyToMany, ManyToOne, OneToMany, JoinColumn } from 'typeorm';

@Entity('Collection')
class Collection{
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({unique: true})
    public name: string;

    @Column({nullable: true})
    public description: string;

    @Column({nullable: true})
    public image_url: string;

    @Column({default: true})
    public is_active: boolean;

    @OneToMany(() => Asset, asset => asset.id)
    assets: Asset[];

}

export default Collection;