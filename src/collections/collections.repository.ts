import {EntityRepository,MetadataAlreadyExistsError,Repository} from 'typeorm';
import Collection from './collections.entities';
import {GetCollectionDto} from './interfaces/getCollection.dto'
import {GetCollectionsDto} from './interfaces/getCollections.dto';
import {CreateCollection} from './interfaces/createCollection.dto'

@EntityRepository(Collection)
export class CollectionRepository extends Repository<Collection>{
    createCollection = async (createCollectionDto: CreateCollection) =>{
        try{
            const collection = this.create( createCollectionDto );
            await this.save( collection );
            return collection;
        }catch( error ){
            //collection already exist 
            const collection = await this.findOne({name: createCollectionDto.name});
            return collection;
        }
    }

    getCollection = async( getCollectionDto: GetCollectionDto) => {
        const collection =  await this.findOne({id: <number><unknown>getCollectionDto.id});
        return collection;
    }

    getCollections = async( getCollectionsDto: GetCollectionsDto) => {
        const {limit,offset} = getCollectionsDto;

        const { sort_key, sort_value } = getCollectionsDto;
        delete getCollectionsDto.limit 
        delete getCollectionsDto.offset
        delete getCollectionsDto.sort_key
        delete getCollectionsDto.sort_value
        let obj:any = {where: getCollectionsDto} ;
        if(limit){
            obj.take = limit;
        }
        if(offset){
            obj.skip  = offset;
        }
        if(sort_key){
            obj.order = { sort_key: 'ASC'}
        }
        if( sort_value ){
           if(sort_key == 'asc'){
                obj.order.sort_key = 'ASC'
           }else{
               obj.order.sort_key = 'DESC';
           }
        }
        let query =  this.createQueryBuilder("collection")
                     .limit(<number><unknown>limit)
                     .offset(<number><unknown>offset)
        if( getCollectionsDto.search_string){
            query = query.andWhere("to_tsvector(collection.name) @@ to_tsquery(:query_string)",{query_string: getCollectionsDto.search_string})
                    .orWhere("to_tsvector(collection.description) @@ to_tsquery(:query_string)",{query_string: getCollectionsDto.search_string})
        }
        console.log(query.getSql())
        const [collections,count] =  await query
                            .getManyAndCount();
        return collections;
    }
}