import { ApiProperty } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty, IsNumber, IsOptional} from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Users } from '../../users/users.entities';

export class GetCollectionDto{
    @IsString()
    @ApiPropertyOptional()
    @IsOptional()
    id: string;

    @IsString()
    @ApiPropertyOptional()
    @IsOptional()
    owner: Users; //the address owner of the current collection
}