import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty, IsNumber, IsOptional} from 'class-validator';
import { Users } from '../../users/users.entities';

export class GetCollectionsDto{

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    name: string;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    owner: Users;

    @ApiPropertyOptional()
    limit: string;

    @ApiPropertyOptional()
    offset: string;

    @ApiPropertyOptional()
    sort_key: string;

    @ApiPropertyOptional()
    sort_value: string;

    @ApiPropertyOptional()
    search_string: string;
}