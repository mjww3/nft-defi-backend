import { ApiProperty } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty, IsNumber, IsOptional, IsEmpty, IsBoolean} from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Users } from '../../users/users.entities'
import { ManyToMany, ManyToOne, OneToOne } from 'typeorm';

export class CreateCollection{
    @IsString()
    @ApiProperty()
    @IsNotEmpty()
    name: string; 
    
    @IsString()
    @ApiPropertyOptional()
    @IsOptional()
    description: string;

    @IsString()
    @ApiPropertyOptional()
    @IsOptional()
    image_url: string;

    @IsBoolean()
    @IsOptional()
    @ApiPropertyOptional()
    is_active: boolean;
}