import { Injectable } from '@nestjs/common';
import {Connection} from 'typeorm'
import Collection from '../collections.entities';
import { CollectionRepository } from '../collections.repository';

import { GetCollectionDto } from '../interfaces/getCollection.dto';
import { GetCollectionsDto } from '../interfaces/getCollections.dto';

@Injectable()
export class CollectionService {
    //get asset
    private collectionRepository: CollectionRepository;
    constructor( private readonly connection: Connection ){
        this.collectionRepository = this.connection.getCustomRepository(CollectionRepository);
    }

    async getCollection( getCollectionDto: GetCollectionDto ) : Promise<Collection>{
        return await this.collectionRepository.getCollection( getCollectionDto );
    }

    async getCollections( getCollectionsDto: GetCollectionsDto ) :Promise<Collection[]>{
        return await this.collectionRepository.getCollections( getCollectionsDto );
    }

}
