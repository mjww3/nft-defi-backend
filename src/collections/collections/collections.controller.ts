import { Controller, Get, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { GetCollectionDto } from '../interfaces/getCollection.dto';
import { GetCollectionsDto } from '../interfaces/getCollections.dto';
import {CollectionService}  from './collections.service';
import { ApiBadGatewayResponse,ApiBadRequestResponse, ApiResponse, ApiTags } from "@nestjs/swagger";

@ApiTags("Collections")
@Controller()
export class CollectionsController{
    constructor( private readonly collectionService: CollectionService){}

    //get an asset
    @ApiResponse({ status: 200 })
    @ApiBadRequestResponse({status:400})
    @ApiBadGatewayResponse({status: 401})
    @Get('/collection')
    @UsePipes(new ValidationPipe())
    async getCollection( @Query() getCollectionDto: GetCollectionDto ){
        const collection = await this.collectionService.getCollection(getCollectionDto );
        return { data: collection }
    }


    @ApiResponse({ status: 200 })
    @ApiBadRequestResponse({status:400})
    @ApiBadGatewayResponse({status: 401})
    @Get('/collections')
    @UsePipes(new ValidationPipe())
    async getCollections( @Query() getCollectionsDto: GetCollectionsDto ){
        const collections = await this.collectionService.getCollections(getCollectionsDto );
        return { data: collections }
    }
    
}