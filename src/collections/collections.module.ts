import { Module } from '@nestjs/common';
import { CollectionService } from './collections/collections.service';
import { CollectionsController } from './collections/collections.controller';
import { CollectionRepository } from './collections.repository';
import {TypeOrmModule} from '@nestjs/typeorm'
import Collections from './collections.entities';
import { Users } from '../users/users.entities';
import { UserRepository } from '../users/users.repository';
import { UtilsService } from '../utils/utils.service';

@Module({
  imports: [TypeOrmModule.forFeature([Collections, CollectionRepository,Users, UserRepository])],
  providers: [CollectionService],
  controllers: [CollectionsController]
})
export class CollectionsModule {}
