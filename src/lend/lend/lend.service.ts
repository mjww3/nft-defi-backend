import {
  //   BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AssetRepository } from '../../assets/assets.repository';
import { Connection } from 'typeorm';
import { CancelOfferDto } from '../interfaces/cancelOffer.dto';
import { CreateOfferDto } from '../interfaces/createOffer.dto';
import { GetOfferDto } from '../interfaces/getOffer.dto';
import { GetOffersDto } from '../interfaces/getOffers.dto';
import { GetActiveOfferDto } from '../interfaces/getActiveOffer.dto';
import Lend from '../lend.entities';
import { LendRepository } from '../lend.repository';
// import abiDecoder = require('abi-decoder');
// const wethAbi = require('../../abi/weth.json');

// abiDecoder.addABI(wethAbi);
// import web3 from '../../utils/web3.client';
import { UserRepository } from '../../users/users.repository';
// import { isURL } from 'class-validator';
import { GetOffersByUserIdDto } from '../interfaces/getOffersByUserId.dto';

@Injectable()
export class LendService {
  //get asset
  private lendRepository: LendRepository;
  private assetRepository: AssetRepository;
  private userRepository: UserRepository;
  lending_data_contract: string;

  constructor(
    private readonly connection: Connection,
    @InjectRepository(AssetRepository) assetRepository: AssetRepository,
    @InjectRepository(UserRepository) userRepository: UserRepository,
  ) {
    this.lendRepository = this.connection.getCustomRepository(LendRepository);
    this.assetRepository = assetRepository;
    this.userRepository = userRepository;
    this.lending_data_contract = '0xF6646bCC8ae654bcC84F4B272084509727AFa9c9';
  }

  //   public async decode_tx_hash(hash: string): Promise<any> {
  //     return new Promise((resolve, reject) => {
  //       console.log(hash);
  //       web3.web3.eth.getTransaction(hash, function (error, response) {
  //         if (error) {
  //           console.log(error);
  //           return reject(error);
  //         } else {
  //           const decodedData = abiDecoder.decodeMethod(response.input);

  //           return resolve(decodedData);
  //         }
  //       });
  //     });
  //   }

  async has_approval(user_id: number): Promise<any> {
    const user = await this.userRepository.getUserById({
      id: user_id.toString(),
    });
    //check if the approval_tx_hash exists
    const has_approval = await this.lendRepository.getIsApproved(user);
    if (!has_approval) {
      return false;
    } else {
      return true;
    }
  }

  //function to mark all the offers of an asset as expired
  async markAllOffersExpired(asset_id: number): Promise<any> {
    const asset = await this.assetRepository.findOne({ id: asset_id });
    if (asset) {
      //else it has been found
    } else {
      return new NotFoundException('Asset Id not found');
    }
  }

  //function to approve the loan
  //   async add_loan_approval_tx(tx_id: string, offer_id: number): Promise<any> {}

  //function to get the offers for the user id
  async get_offers_user_id(
    getOfferIdsDto: GetOffersByUserIdDto,
    user_id: number,
  ): Promise<any> {
    const user = await this.userRepository.getUserById({
      id: user_id.toString(),
    });
    if (!user) {
      return new NotFoundException('User not found');
    }
    const offers = await this.lendRepository.getOffersUserId(
      user,
      getOfferIdsDto.offset ? <number>(<unknown>getOfferIdsDto.offset) : 0,
      getOfferIdsDto.limit ? <number>(<unknown>getOfferIdsDto.limit) : 10,
    );
    return offers;
  }

  //function to get the approved loans by user_id
  async get_approved_loans(
    getOfferIdsDto: GetOffersByUserIdDto,
    user_id: number,
  ): Promise<any> {
    const user = await this.userRepository.getUserById({
      id: user_id.toString(),
    });
    if (!user) {
      return new NotFoundException('user not found');
    }
    const loans = await this.lendRepository.getLoansUserId(
      user,
      getOfferIdsDto.offset ? <number>(<unknown>getOfferIdsDto.offset) : 0,
      getOfferIdsDto.limit ? <number>(<unknown>getOfferIdsDto.limit) : 10,
    );
    return loans;
  }

  async get_active_offer(
    getActiveOfferDto: GetActiveOfferDto,
    user_id: number,
  ): Promise<any> {
    const user = await this.userRepository.getUserById({
      id: user_id.toString(),
    });
    if (!user) {
      return new NotFoundException('user not found');
    }
    const loans = await this.lendRepository.getAssetActiveOffer(
      user,
      getActiveOfferDto.asset_id,
    );
    return loans;
  }

  async createOffer(
    createOfferDto: CreateOfferDto,
    user_id: number,
  ): Promise<any> {
    let offer;
    const user = await this.userRepository.getUserById({
      id: user_id.toString(),
    });
    if (!user) {
      return new UnauthorizedException('User not found');
    }
    const asset = await this.assetRepository.getAsset({
      id: createOfferDto.asset_id.toString(),
    });

    if (!asset) {
      return new InternalServerErrorException(
        'Assets not found please check your assets ids!',
      );
    }

    if (asset) {
      try {
        offer = await this.lendRepository.create({
          amount: createOfferDto.amount,
          asset: asset,
          currency: createOfferDto.currency,
          expiry:
            parseInt((Date.now() / 1000).toString()) +
            createOfferDto.expiry * 24 * 60 * 60,
          status: 'ACTIVE',
          interest_rate: createOfferDto.interest_rate,
          user: user,
        });
        this.lendRepository.save(offer);
      } catch (error) {
        return new InternalServerErrorException('Error creating offer');
      }
    }
    return offer;
  }

  //   async createOffer(
  //     createOfferDto: CreateOfferDto,
  //     user_id: number,
  //   ): Promise<any> {
  //     let amount;
  //     let offer;
  //     let is_valid = false;
  //     if (createOfferDto.approve_tx_hash) {
  //       const tx_hash = createOfferDto.approve_tx_hash;
  //       const decodedata = await this.decode_tx_hash(tx_hash);
  //       if (decodedata.name == 'approve') {
  //         const params = decodedata.params;
  //         params.map((param) => {
  //           //also check for the contract address should be the approver
  //           if (param.name == 'to') {
  //             //it is a valid approval transaction
  //             is_valid = true;
  //           } else if (param.name == 'amount') {
  //             amount = param.value;
  //           }
  //         });
  //         if (is_valid) {
  //           const user = await this.userRepository.getUserById({
  //             id: user_id.toString(),
  //           });
  //           if (!user) {
  //             return new UnauthorizedException('User not found');
  //           }
  //           const asset = await this.assetRepository.getAsset({
  //             id: createOfferDto.asset_id.toString(),
  //           });
  //           if (asset) {
  //             try {
  //               offer = await this.lendRepository.create({
  //                 approve_tx_hash: createOfferDto.approve_tx_hash,
  //                 amount: createOfferDto.amount,
  //                 asset: asset,
  //                 currency: createOfferDto.currency,
  //                 expiry:
  //                   parseInt((Date.now() / 1000).toString()) +
  //                   createOfferDto.expiry * 24 * 60 * 60,
  //                 status: 'ACTIVE',
  //                 interest_rate: createOfferDto.interest_rate,
  //                 user: user,
  //               });
  //               this.lendRepository.save(offer);
  //             } catch (error) {
  //               return new InternalServerErrorException('Error creating offer');
  //             }
  //           } else {
  //             return new NotFoundException('asset not found');
  //           }
  //         } else {
  //           return new BadRequestException('Invalid transaction');
  //         }
  //         return offer;
  //       } else {
  //         return new BadRequestException('Invalid transaction');
  //       }
  //     } else {
  //       const user = await this.userRepository.getUserById({
  //         id: user_id.toString(),
  //       });
  //       //check if the approval_tx_hash exists
  //       const has_approval = await this.lendRepository.getIsApproved(user);
  //       if (!has_approval) {
  //         return new BadRequestException('Approval for user not found');
  //       }
  //       if (!user) {
  //         return new UnauthorizedException('User not found');
  //       }
  //       const asset = await this.assetRepository.getAsset({
  //         id: createOfferDto.asset_id.toString(),
  //       });
  //       if (asset) {
  //         try {
  //           offer = await this.lendRepository.create({
  //             approve_tx_hash: createOfferDto.approve_tx_hash,
  //             amount: createOfferDto.amount,
  //             asset: asset,
  //             currency: createOfferDto.currency,
  //             expiry:
  //               parseInt((Date.now() / 1000).toString()) +
  //               createOfferDto.expiry * 24 * 60 * 60,
  //             status: 'ACTIVE',
  //             interest_rate: createOfferDto.interest_rate,
  //             user: user,
  //           });
  //           this.lendRepository.save(offer);
  //         } catch (error) {
  //           return new InternalServerErrorException('Error creating offer');
  //         }
  //         return offer;
  //       } else {
  //         return new NotFoundException('asset not found');
  //       }
  //     }
  //   }

  async getOffers(getOffersDto: GetOffersDto): Promise<Lend[]> {
    return await this.lendRepository.getOffers(getOffersDto);
  }

  async getOffer(getOfferDto: GetOfferDto): Promise<Lend> {
    return await this.lendRepository.getOffer(getOfferDto);
  }

  async cancelOffer(
    cancelOfferDto: CancelOfferDto,
    user_id: number,
  ): Promise<any> {
    return await this.lendRepository.cancelOffer(cancelOfferDto, user_id);
  }
}
