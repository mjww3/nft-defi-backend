import { LendService } from './lend.service';
import {
  ApiBadGatewayResponse,
  ApiBadRequestResponse,
  ApiResponse,
  ApiTags,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  Body,
  Controller,
  Get,
  Headers,
  InternalServerErrorException,
  Post,
  Query,
  UnauthorizedException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { GetOfferDto } from '../interfaces/getOffer.dto';
import { GetOffersDto } from '../interfaces/getOffers.dto';
import { CreateOfferDto } from '../interfaces/createOffer.dto';
import { CancelOfferDto } from '../interfaces/cancelOffer.dto';
import jwtHelper from '../../helpers/jwt';
import { GetOffersByUserIdDto } from '../interfaces/getOffersByUserId.dto';
import { GetActiveOfferDto } from '../interfaces/getActiveOffer.dto';

@ApiTags('Lends')
@Controller()
export class LendController {
  constructor(private readonly lendService: LendService) {}

  @ApiResponse({ status: 200 })
  @ApiBadRequestResponse({ status: 400 })
  @ApiBadGatewayResponse({ status: 401 })
  @Get('/offers')
  @UsePipes(new ValidationPipe())
  async getOffers(@Query() getOffersDto: GetOffersDto) {
    const collection = await this.lendService.getOffers(getOffersDto);
    return { data: collection };
  }

  @ApiResponse({ status: 200 })
  @ApiBadRequestResponse({ status: 400 })
  @ApiBadGatewayResponse({ status: 401 })
  @Get('/offer')
  @UsePipes(new ValidationPipe())
  async getOffer(@Query() getOfferDto: GetOfferDto) {
    const collection = await this.lendService.getOffer(getOfferDto);
    return { data: collection };
  }

  @ApiResponse({ status: 200 })
  @ApiBadRequestResponse({ status: 400 })
  @ApiUnauthorizedResponse({ status: 401 })
  @ApiBadGatewayResponse({ status: 401 })
  @Post('/createoffer')
  @UsePipes(new ValidationPipe())
  async createOffer(
    @Body() createOfferDto: CreateOfferDto,
    @Headers() headers,
  ) {
    const authorization = headers['authorization'];
    if (!authorization) {
      return new UnauthorizedException('unauthorized');
    }
    //now get the user id after decoding the jwt
    const data = await jwtHelper.verifyJWT(authorization);
    console.log('data', data);
    if (data.error && !data.data) {
      return new UnauthorizedException('unauthorized');
    } else {
      const decoded = data.data;
      if (!decoded.data && !decoded.data.id) {
        return new InternalServerErrorException('Internal server error');
      }
      const collection = await this.lendService.createOffer(
        createOfferDto,
        decoded.data.id,
      );
      return { data: collection };
    }
  }

  @ApiResponse({ status: 200 })
  @ApiBadRequestResponse({ status: 400 })
  @ApiUnauthorizedResponse({ status: 401 })
  @ApiBadGatewayResponse({ status: 401 })
  @Get('/getOffersByUser')
  @UsePipes(new ValidationPipe())
  async getOffersByUserId(
    @Query() getOffersByUserIdDto: GetOffersByUserIdDto,
    @Headers() headers,
  ) {
    const authorization = headers['authorization'];
    if (!authorization) {
      return new UnauthorizedException('unauthorized');
    }
    //now get the user id after decoding the jwt
    const data = await jwtHelper.verifyJWT(authorization);
    if (data.error && !data.data) {
      return new UnauthorizedException('unauthorized');
    } else {
      const decoded = data.data;
      if (!decoded.data && !decoded.data.id) {
        return new InternalServerErrorException('Internal server error');
      }
      const offers = await this.lendService.get_offers_user_id(
        getOffersByUserIdDto,
        decoded.data.id,
      );
      return { data: offers };
    }
  }

  @ApiResponse({ status: 200 })
  @ApiBadRequestResponse({ status: 400 })
  @ApiUnauthorizedResponse({ status: 401 })
  @ApiBadGatewayResponse({ status: 401 })
  @Get('/getLoansByUser')
  @UsePipes(new ValidationPipe())
  async GetFinalizedLoansByUser(
    @Query() getOffersByUserIdDto: GetOffersByUserIdDto,
    @Headers() headers,
  ) {
    const authorization = headers['authorization'];
    if (!authorization) {
      return new UnauthorizedException('unauthorized');
    }
    //now get the user id after decoding the jwt
    const data = await jwtHelper.verifyJWT(authorization);
    if (data.error && !data.data) {
      return new UnauthorizedException('unauthorized');
    } else {
      const decoded = data.data;
      if (!decoded.data && !decoded.data.id) {
        return new InternalServerErrorException('Internal server error');
      }
      const offers = await this.lendService.get_approved_loans(
        getOffersByUserIdDto,
        decoded.data.id,
      );
      return { data: offers };
    }
  }

  @ApiResponse({ status: 200 })
  @ApiBadRequestResponse({ status: 400 })
  @ApiUnauthorizedResponse({ status: 401 })
  @ApiBadGatewayResponse({ status: 401 })
  @Get('/isApproved')
  @UsePipes(new ValidationPipe())
  async isApproved(@Headers() headers) {
    const authorization = headers['authorization'];
    if (!authorization) {
      return new UnauthorizedException('unauthorized');
    }
    //now get the user id after decoding the jwt
    const data = await jwtHelper.verifyJWT(authorization);
    if (data.error && !data.data) {
      return new UnauthorizedException('unauthorized');
    } else {
      const decoded = data.data;
      if (!decoded.data && !decoded.data.id) {
        return new InternalServerErrorException('Internal server error');
      }
      const isApproved = await this.lendService.has_approval(decoded.data.id);
      return { data: isApproved };
    }
  }

  @ApiResponse({ status: 200 })
  @ApiBadRequestResponse({ status: 400 })
  @ApiUnauthorizedResponse({ status: 401 })
  @ApiBadGatewayResponse({ status: 401 })
  @Get('/getActiveOffer')
  @UsePipes(new ValidationPipe())
  async getActiveOffer(
    @Query() getActiveOfferDto: GetActiveOfferDto,
    @Headers() headers,
  ) {
    const authorization = headers['authorization'];

    if (!authorization) {
      return new UnauthorizedException('unauthorized');
    }
    //now get the user id after decoding the jwt
    const data = await jwtHelper.verifyJWT(authorization);

    if (data.error && !data.data) {
      return new UnauthorizedException('unauthorized');
    } else {
      const decoded = data.data;
      if (!decoded.data && !decoded.data.id) {
        return new InternalServerErrorException('Internal server error');
      }
      const getActiveOfferData = await this.lendService.get_active_offer(
        getActiveOfferDto,
        decoded.data.id,
      );
      return { data: getActiveOfferData };
    }
  }

  @ApiResponse({ status: 200 })
  @ApiBadRequestResponse({ status: 400 })
  @ApiBadGatewayResponse({ status: 401 })
  @Post('/cancelOffer')
  @UsePipes(new ValidationPipe())
  async cancelOffer(
    @Body() cancelOfferDto: CancelOfferDto,
    @Headers() headers,
  ) {
    const authorization = headers['authorization'];
    if (!authorization) {
      return new UnauthorizedException('unauthorized');
    }
    const data = await jwtHelper.verifyJWT(authorization);
    if (data.error && !data.data) {
      return new UnauthorizedException('unauthorized');
    } else {
      const decoded = data.data;
      if (!decoded.data && !decoded.data.id) {
        return new InternalServerErrorException('Internal server error');
      }
      const collection = await this.lendService.cancelOffer(
        cancelOfferDto,
        decoded.data.id,
      );
      return { data: collection };
    }
  }
}
