import { Module } from '@nestjs/common';
import { LendService } from './lend/lend.service';
import { LendController } from './lend/lend.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Users } from '../users/users.entities';
import { UserRepository } from '../users/users.repository';
import Lend from './lend.entities';
import { Borrow } from '../borrow/borrows.entities';
import { BorrowRepository } from '../borrow/borrow.repository';
import { Asset } from '../assets/assets.entities';
import { AssetRepository } from '../assets/assets.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Lend,
      Users,
      UserRepository,
      Borrow,
      BorrowRepository,
      Asset,
      AssetRepository,
    ]),
  ],
  providers: [LendService],
  controllers: [LendController],
})
export class LendModule {}
