import { ApiPropertyOptional } from '@nestjs/swagger';

//Get offers dto
export class GetOffersByUserIdDto {
  @ApiPropertyOptional()
  limit: string;

  @ApiPropertyOptional()
  offset: string;
}
