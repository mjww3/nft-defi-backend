import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsDefined, IsNumber } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

export class CreateOfferDto {
  @ApiPropertyOptional()
  approve_tx_hash: string;

  @IsString()
  @ApiProperty()
  amount: string;

  @IsNumber()
  @IsDefined()
  @ApiProperty()
  asset_id: number; //asset id on which the offer is created //convert to number after reading

  @IsString()
  @IsDefined()
  @ApiProperty()
  interest_rate: string;

  @IsString()
  @IsDefined()
  @ApiProperty()
  currency: string;

  @IsNumber()
  @IsDefined()
  @ApiProperty()
  expiry: number;
}
