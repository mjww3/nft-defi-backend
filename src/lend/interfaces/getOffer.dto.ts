import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsDefined } from 'class-validator';

export class GetOfferDto {
  @IsString()
  @IsDefined()
  @ApiProperty()
  offer_id: string;
}
