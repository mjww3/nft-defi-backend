import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsDefined } from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';

//Get offers dto
export class GetOffersDto {
  @IsString()
  @IsDefined()
  @ApiProperty()
  asset_id: string;

  @ApiPropertyOptional()
  limit: string;

  @ApiPropertyOptional()
  offset: string;
}
