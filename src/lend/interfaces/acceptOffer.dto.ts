import { IsString, IsDefined } from 'class-validator';

//Accept offer dto
export class AcceptOfferDto {
  @IsString()
  @IsDefined()
  offer_id: string;
}
