import { IsString, IsDefined } from 'class-validator';

export class CancelOfferDto {
  @IsString()
  @IsDefined()
  offer_id: string;
}
