import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

//Get offers dto
export class UpdateOfferDto {
  @IsString()
  @ApiProperty()
  bid_id: string;

  @IsString()
  @ApiProperty()
  status: string;
}
