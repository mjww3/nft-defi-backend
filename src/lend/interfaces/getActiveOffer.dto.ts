import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

//Get offers dto
export class GetActiveOfferDto {
  @IsString()
  @ApiProperty()
  asset_id: string;
}
