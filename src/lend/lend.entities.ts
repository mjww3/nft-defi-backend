import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Asset } from '../assets/assets.entities';
import { Users } from '../users/users.entities';
@Entity('lends')
///the code to create the lends
class Lend {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne(() => Asset, (asset) => asset.id)
  @JoinColumn()
  public asset: Asset; //the assetid of the asset for which transaction is being done

  @Column()
  public status: string; // ACTIVE / ACCEPTED / CANCELLED / COMPLETED

  @Column({ type: 'jsonb', nullable: true })
  // eslint-disable-next-line @typescript-eslint/ban-types
  public meta: object;

  @Column({ nullable: false })
  public amount: string;

  @Column()
  public currency: string;

  @Column({ unique: false, nullable: true })
  public approve_tx_hash: string;

  @ManyToOne(() => Users, (user) => user.id)
  @JoinColumn()
  public user: Users;

  @Column()
  interest_rate: string;

  @Column()
  //expiry in days
  expiry: number;

  @Column({ nullable: true })
  loan_approval_tx_id: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Lend;
