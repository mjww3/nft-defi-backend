import { EntityRepository, IsNull, Not, Repository } from 'typeorm';
import Lend from './lend.entities';
import { GetOffersDto } from './interfaces/getOffers.dto';
import { CancelOfferDto } from './interfaces/cancelOffer.dto';
import { GetOfferDto } from './interfaces/getOffer.dto';
import { UpdateOfferDto } from './interfaces/updateOffer.dto';
import { BadRequestException } from '@nestjs/common';

//assets
@EntityRepository(Lend)
export class LendRepository extends Repository<Lend> {
  constructor() {
    super();
  }

  getOffers = async (getOffersDto: GetOffersDto) => {
    const { limit, offset } = getOffersDto;
    const query = this.createQueryBuilder('lend')
      .limit(<number>(<unknown>limit))
      .where('lend.asset = :asset_id', { asset_id: getOffersDto.asset_id })
      .andWhere('lend.status != :status', { status: 'EXPIRED' })
      .andWhere('lend.status != :status', { status: 'CANCELLED' })
      .andWhere('lend.expiry > :now', {
        now: parseInt((Date.now() / 1000).toString()),
      })
      .offset(<number>(<unknown>offset));
    const [offers, count] = await query.getManyAndCount();
    return offers;
  };

  getIsApproved = async (user: any) => {
    const approvedOffer = await this.findOne({
      user: user,
      approve_tx_hash: Not(IsNull()),
    });
    if (approvedOffer) {
      return true;
    } else {
      return false;
    }
  };

  markAllOffersExpired = async (asset: any) => {
    return null;
  };

  getLoansUserId = async (user: any, offset: number, limit: number) => {
    const loans = await this.find({
      where: { user: user, loan_approval_tx_id: Not(IsNull()) },
      skip: offset,
      take: limit,
    });
    return loans;
  };

  getAssetActiveOffer = async (user: any, asset: string) => {
    const offers = await this.findOne({
      where: { user: user, asset, status: 'ACTIVE' },
    });
    return offers;
  };

  getOffersUserId = async (user: any, offset: number, limit: number) => {
    const query = this.createQueryBuilder('lend')
      .innerJoinAndSelect('lend.asset', 'asset')
      .where('lend.user  = :user', { user: user.id })
      .limit(<number>(<unknown>limit))
      .offset(<number>(<unknown>offset));
    const [offers, count] = await query.getManyAndCount();
    return offers;
  };

  getOffer = async (getOfferDto: GetOfferDto) => {
    const query = this.createQueryBuilder('lend').where('lend.id = :offer_id', {
      offer_id: getOfferDto.offer_id,
    });
    console.log(query.getSql());
    const offer = await query.getOne();
    return offer;
  };

  updateOfferStatus = async (updateOfferStatus: UpdateOfferDto) => {
    const offer = await this.findOne({
      where: { id: updateOfferStatus.bid_id },
    });
    return this.save({
      ...offer,
      status: updateOfferStatus.status,
    });
  };

  cancelOffer = async (cancelOfferDto: CancelOfferDto, user_id: number) => {
    const offer = await this.findOne({
      where: { id: cancelOfferDto.offer_id, user: user_id },
    });
    if (!offer) {
      return new BadRequestException('Offer does not belong to user');
    } else {
      return this.save({ ...offer, status: 'CANCELLED' });
    }
  };
}
