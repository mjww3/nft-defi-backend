import { Borrow } from '../borrow/borrows.entities';
import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  BaseEntity,
  OneToMany,
} from 'typeorm';
import { Asset } from '../assets/assets.entities';
// import Collection from '../collections/collections.entities';
// import Transaction from '../transactions/transactions.entities';

@Entity('users')
export class Users {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public name: string;

  @Column({ unique: true })
  public address: string;

  @Column()
  public blockchain: string;

  @Column({ nullable: true })
  public profile_pic: string;

  @Column({ nullable: true })
  public current_nonce: string;

  @OneToMany(() => Asset, (asset) => asset.owner)
  assets: Asset[];

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  created_at: string;
}
