import { Module } from '@nestjs/common';
import { UsersService } from './users/users.service';
import { UsersController } from './users/users.controller';
import {UserRepository} from './users.repository';
import {Users} from './users.entities'
import {TypeOrmModule} from '@nestjs/typeorm'
import { BorrowRepository } from '../borrow/borrow.repository';
import { Borrow } from '../borrow/borrows.entities';
import { Asset } from '../assets/assets.entities';
import Collection from '../collections/collections.entities';

@Module({
  imports: [TypeOrmModule.forFeature([Users, UserRepository,Borrow,BorrowRepository,Asset,Collection])],
  providers: [UsersService,UserRepository],
  controllers: [UsersController]
})
export class UsersModule {}
