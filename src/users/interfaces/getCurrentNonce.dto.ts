import { ApiProperty } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty} from 'class-validator';

export class getCurrentNonce {
    @IsString()
    @ApiProperty()
    @IsDefined()
    @IsNotEmpty()
    address: string;
}