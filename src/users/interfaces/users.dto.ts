import {IsString, IsDefined, IsNotEmpty, IsEmpty, IsOptional} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UsersDto {
     @IsString()
     @IsOptional()
     id?:number;

     @IsString()
     @ApiProperty()
     @IsOptional()
     name?: string;

     @IsString()
     @ApiProperty()
     @IsDefined()
     @IsNotEmpty()
     address: string;

     @IsString()
     @ApiProperty()
     @IsOptional()
     blockchain?: string;

     @IsString()
     @ApiProperty()
     @IsOptional()
     profile_pic?: string;
}