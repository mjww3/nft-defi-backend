import {IsString, IsDefined, IsNotEmpty} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class VerifySignatureDto {
    @IsString()
    @ApiProperty()
    @IsDefined()
    address: string;

    @IsString()
    @IsDefined()
    @ApiProperty()
    signature: string;
}