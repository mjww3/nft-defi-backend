import {IsString, IsDefined, IsNotEmpty} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetUserDto {
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    address: string;
}