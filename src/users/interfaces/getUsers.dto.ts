import {IsString, IsDefined, IsNotEmpty} from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class getUsersDto {
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    id: string;
}