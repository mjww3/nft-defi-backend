import {IsString, IsDefined, IsNotEmpty} from 'class-validator';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateUsersDto {
    @IsString()
    @ApiPropertyOptional()
    name: string;

    @IsString()
    @ApiPropertyOptional()
    profile_pic?: string;

    @IsString()
    @ApiProperty()
    id: number;
}