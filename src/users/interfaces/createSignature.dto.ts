import { ApiProperty } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty} from 'class-validator';

export class CreateSignatureDto {
    @IsString()
    @ApiProperty()
    @IsDefined()
    @IsNotEmpty()
    nonce: string;

    @IsString()
    @ApiProperty()
    @IsDefined()
    @IsNotEmpty()
    private_key: string;
}