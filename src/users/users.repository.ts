import {Users} from './users.entities'
import {UsersDto} from './interfaces/users.dto';
import {EntityRepository,MetadataAlreadyExistsError,Repository} from 'typeorm';
import { getUsersDto } from './interfaces/getUsers.dto';
import { UpdateUsersDto } from './interfaces/updateUsers.dto';
import { BadRequestException, HttpException, NotFoundException } from '@nestjs/common';
var voucher_codes = require('voucher-code-generator');
const BLOCKCHAIN_SUPPORTED = ["ETHEREUM", "BINANCE_CHAIN", "MATIC"];
import {GetUserDto} from './interfaces/getUser.dto'
//users
@EntityRepository(Users)
export class UserRepository extends Repository<Users>{
    createUser = async (usersDto: UsersDto) => {
        usersDto.address = usersDto.address.toLowerCase();
        const blockchain = usersDto.blockchain;
        console.log(BLOCKCHAIN_SUPPORTED.indexOf(blockchain))
        if(BLOCKCHAIN_SUPPORTED.indexOf(blockchain)>=0){
            const user = this.create(usersDto);
            try{
                await this.save(user);
                return user;
            }catch( error ){
                console.log(error)
                throw new BadRequestException('Already exists');
            }
        }else{
             throw new BadRequestException('Bad request');
        }
    }

    getUser = async ( getUserDto : GetUserDto ) =>{
        getUserDto.address = getUserDto.address.toLowerCase();
        const user =  await this.findOne(getUserDto);
        if(!user){
            throw new NotFoundException('User not found')
        }else{
            return user;
        }
    }

    getUserById = async ( getusersDto : getUsersDto ) =>{
        const user =  await this.findOne({id: <number><unknown>getusersDto.id});
        if(!user){
            throw new NotFoundException('User not found')
        }else{
            return user;
        }
    }

    //will be used to update the profile
    updateUser = async ( id: number, updateUserDto: UpdateUsersDto ) =>{
        const user =  await this.update(id, updateUserDto).then(response => response.raw[0]);
        return user;
    }
    
    getCurrentNonce = async( getusersDto: getUsersDto ) => {
        const user =  await this.findOne( {id: <number><unknown>getusersDto.id} );
        if( ! user ){
            throw new NotFoundException('Nonce not set')
        }else{
            return user.current_nonce;
        }
    }

    createNonce = async( getusersDto:getUsersDto ) =>{
        //check if address belongs to any user yet
        const user = await this.findOne({id: <number><unknown>getusersDto.id});
        if(!user) return null;
        const random_nonce = voucher_codes.generate({
            length: 8,
            count: 1
        });
        await this.update( {id: <number><unknown>getusersDto.id}, { current_nonce: random_nonce[0] })
        return random_nonce[0];
    }

}