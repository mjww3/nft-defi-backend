import { Body, Controller, Delete, Get, HttpStatus, Post, Put, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { getCurrentNonce } from '../interfaces/getCurrentNonce.dto';
import { getUsersDto } from '../interfaces/getUsers.dto';
import { UpdateUsersDto } from '../interfaces/updateUsers.dto';
import { UsersDto } from '../interfaces/users.dto';
import  { UsersService } from './users.service';
import {VerifySignatureDto} from '../interfaces/verifySignature.dto';
import { CreateSignatureDto } from '../interfaces/createSignature.dto';
import { ApiBadGatewayResponse, ApiResponse, ApiTags } from "@nestjs/swagger";
import { GetUserDto } from '../interfaces/getUser.dto';

enum BLOCKCHAIN_SUPPORTED {
    ETHEREUM= "ETHEREUM",
    BINANCE_CHAIN="BINANCE_CHAIN",
    MATIC="MATIC"
};

@ApiTags("Users")
@Controller()
export class UsersController {
    constructor(private readonly userService: UsersService){}

    @ApiResponse({ status: 200 })
    @Get("/user")
    @UsePipes(new ValidationPipe())
    async getUsers(@Query() getUserDto: GetUserDto){
        const user = await this.userService.getUser(getUserDto);
        return { data: user }
    }

    @ApiResponse({ status: 201 })
    @Post("/user")
    @UsePipes(new ValidationPipe())
    async createUsers(@Body() usersdto :UsersDto){
        const user =  await this.userService.createUser(usersdto);
        return { data: user }
    }

    @ApiResponse({ status: 200 })
    @Put("/user")
    @UsePipes(new ValidationPipe())
    async updateUsers(@Body() updateUserDto: UpdateUsersDto){
        const id = updateUserDto.id;
        delete updateUserDto.id;
        const user = await this.userService.updateUser(id, updateUserDto);
        return { data: user }
    }

    @ApiResponse({ status: 200 })
    @Get("/getCurrentNonce")
    @UsePipes(new ValidationPipe())
    async getCurrentNonce(@Query() getusersDto: getUsersDto ){
        const nonce = await this.userService.getCurrentNonce(getusersDto);
        return { data: nonce }
    }

    @ApiResponse({ status: 200 })
    @Post("/createNonce")
    @UsePipes(new ValidationPipe())
    async createNonce(@Query() getusersDto: getUsersDto ){
        const nonce = await this.userService.createNonce(getusersDto);
        return { data: nonce }
    }

    @ApiResponse({ status: 200 })
    @Get("/supportedBlockchains")
    @UsePipes(new ValidationPipe())
    async supportedBlockchain(){
        return { data: BLOCKCHAIN_SUPPORTED }
    }


    @ApiResponse({ status: 200 })
    @Post("/verifySignature")
    @UsePipes(new ValidationPipe())
    async verifySignature(@Body() signaturedto: VerifySignatureDto){
        const data = await this.userService.verifySignature(signaturedto.address, signaturedto.signature);
        return { data: data.data, token: data.token }
    }

    @ApiResponse({ status: 200 })
    @Post('/createSignature')
    @UsePipes(new ValidationPipe())
    async createSignature(@Body() createsignaturdto: CreateSignatureDto){
        const signature = await this.userService.createSignature(createsignaturdto);
        return { data: signature }
    }

    @ApiResponse({ status: 200 })
    @ApiBadGatewayResponse({status: HttpStatus.BAD_GATEWAY})
    @Get('/loanbook')
    @UsePipes(new ValidationPipe())
    getLoanBook(@Query() address: string ){
        
    }

}
