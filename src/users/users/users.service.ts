import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import {UserRepository} from '../users.repository';
import {getUsersDto} from '../interfaces/getUsers.dto';
import {UsersDto} from '../interfaces/users.dto'
const Web3 = require('web3');
import { UpdateUsersDto } from '../interfaces/updateUsers.dto';
import { Connection } from 'typeorm';
import { CreateSignatureDto} from '../interfaces/createSignature.dto'
var web3 = new Web3(process.env.INFURA_API||'https://rinkeby.infura.io/v3/a9e4aa3de809420f8d355b764323069b');
const jwt = require('jsonwebtoken');
import {GetUserDto} from '../interfaces/getUser.dto'; 

@Injectable()
export class UsersService {
    private userRepository: UserRepository;
    constructor( private readonly connection: Connection ){
        this.userRepository = this.connection.getCustomRepository(UserRepository);
    }

    async getUser(getUserDto: GetUserDto) : Promise<UsersDto>{
        try{
            return await this.userRepository.getUser( getUserDto )
        }catch( error ){
            throw new HttpException({message: error.message}, HttpStatus.BAD_REQUEST)
        }
    }

    async createUser(createuserdto: UsersDto) :Promise<UsersDto>{
        try{
            return await this.userRepository.createUser( createuserdto);
        }catch( error ){
            throw new HttpException({message: error.message}, HttpStatus.BAD_REQUEST)
        }
    }

    async updateUser(id: number, updateuserdto: UpdateUsersDto) :Promise<UsersDto>{
        try{
            return await this.userRepository.updateUser(id, updateuserdto);
        }catch( error ){
            throw new HttpException({message: error.message}, HttpStatus.BAD_REQUEST)
        }
    }

    //function to return the nonce
    async createNonce( getusersDto: getUsersDto ) :Promise<string>{
        try{
            return await this.userRepository.createNonce(getusersDto);
        }catch( error ){
            throw new HttpException({message: error.message}, HttpStatus.BAD_REQUEST)
        }
    }

    //funciton to create signature
    async createSignature(createsignaturedto: CreateSignatureDto) :Promise<string>{
        try{
            const signature = await web3.eth.accounts.sign(createsignaturedto.nonce, createsignaturedto.private_key);
            return signature.signature;
        }catch( error ){
            throw new HttpException({message: error.message}, HttpStatus.BAD_REQUEST)  
        }
    }

    //function to verify the signature
    async verifySignature(address:string, signature:string) : Promise<any>{
        try{
            address = address.toLowerCase();
            const data = await this.userRepository.findOne({address});
            if( data && data.current_nonce){
                let signingAddress = web3.eth.accounts.recover(data.current_nonce, signature);
                signingAddress = signingAddress.toLowerCase()
                const user = await this.userRepository.findOne({where:{address:address.toLowerCase()}})
                if (user){
                    const token = jwt.sign({
                        exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24* 365),
                        data: user
                      }, process.env.JWT_SECRET || 'secret');
                    return {data:signingAddress.toLowerCase() == address.toLowerCase(),token: token}
                }else{
                    throw new HttpException({message: "USER_NOT_FOUND"}, HttpStatus.NOT_FOUND)
                }
            }else{
                return {data:false,token:null};
            }
        }catch( error ){
            throw new HttpException({message: error.message}, HttpStatus.BAD_REQUEST)   
        }
    }

    //get current nonce
    async getCurrentNonce( getusersDto:getUsersDto ): Promise<string>{
        try{
            return await this.userRepository.getCurrentNonce( getusersDto );
        }catch( error ){
            throw new HttpException({message: error.message}, HttpStatus.BAD_REQUEST);
        }
    }

}
