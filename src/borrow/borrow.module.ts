import { Module } from '@nestjs/common';
import { BorrowService } from './borrow/borrows.service';
import { BorrowController } from './borrow/borrow.controller';
import { Borrow } from './borrows.entities';
import { BorrowRepository } from './borrow.repository';
import {TypeOrmModule} from '@nestjs/typeorm'
import { Users } from '../users/users.entities';
import { Asset } from '../assets/assets.entities';
import {ConfigModule} from '@nestjs/config';
import { UserRepository } from '../users/users.repository';
import { TransactionRepository } from '../transactions/transactions.repository';
import Transaction from '../transactions/transactions.entities';
import { TransactionsService } from '../transactions/transactions.service';
import { UtilsService } from '../utils/utils.service';
import { CollectionRepository } from '../collections/collections.repository';

@Module({
  imports: [ConfigModule.forRoot(),TypeOrmModule.forFeature([Borrow, BorrowRepository,Transaction, TransactionRepository,Users, Asset, UserRepository,CollectionRepository])],
  providers: [BorrowService,UtilsService],
  controllers: [BorrowController]
})
export class BorrowModule {}

