import {AssetContract} from './borrows.entities'
import {EntityRepository,Repository} from 'typeorm';
import { CreateBorrowDto } from './interfaces/createBorrow.dto';
import { GetBorrowDto } from './interfaces/getBorrow.dto';
import { GetBorrowsDto } from './interfaces/getBorrows.dto';
import { CreateAssetContractDto } from './interfaces/createAssetContract.dto';
import { GetAssetContractDto } from './interfaces/getAssetContract.dto';

//assets
@EntityRepository(AssetContract)
export class AssetContractRepository extends Repository<AssetContract>{

    async createAssetContract( createassetcontractdto: CreateAssetContractDto){
        const contract = await this.create( createassetcontractdto );
        await this.save( contract );
        return contract;
    }

    async getAssetConract( getassetcontractdto: GetAssetContractDto ){
        const assetcontract = await this.findOne({where: getassetcontractdto });
        return assetcontract;
    }


}