import {Borrow} from './borrows.entities'
import {EntityRepository,Repository} from 'typeorm';
import { CreateBorrowDto } from './interfaces/createBorrow.dto';
import { GetBorrowDto } from './interfaces/getBorrow.dto';
import { GetBorrowsDto } from './interfaces/getBorrows.dto';
import { Asset } from '../assets/assets.entities';
import { CreateTransactionDto } from '../transactions/interfaces/createTransaction.dto'
import { Logger, NotFoundException } from '@nestjs/common';
//assets
import axios from 'axios';
import  constants  from './borrow/borrow.constants';
import { InjectRepository } from '@nestjs/typeorm';
import { CollectionRepository } from '../collections/collections.repository';

@EntityRepository(Borrow)
export class BorrowRepository extends Repository<Borrow>{
    public RARIBLE_API_GET_META:string ;
    public OPENSEA_GET_SINGLE_ASSET: string;
    public RARIBLE_GET_SINGLE_ASSET: string; 
    public RARIBLE_API_GET_ASSETS :string;
    public OPENSEA_API_GET_ASSETS:string ;
    constructor( @InjectRepository(CollectionRepository) private collectionRepository: CollectionRepository){
        super()
        if (process.env.MODE == "DEV" ||  !process.env.MODE ){
            this.RARIBLE_API_GET_META = constants.RARIBLE_GET_META_RINKEBY;
            this.RARIBLE_API_GET_ASSETS = constants.RARIBLE_API_GET_ASSETS_RINKEBY;
            this.OPENSEA_API_GET_ASSETS = constants.OPENSEA_API_RINKEBY;
            this.OPENSEA_GET_SINGLE_ASSET = constants.OPENSEA_RINKEBY_SINGLE_ASSET;
            this.RARIBLE_GET_SINGLE_ASSET = constants.RARIBLE_API_GET_ASSETS_RINKEBY;
            }else{
            this.RARIBLE_API_GET_META = constants.RARIBLE_API_GET_META;
            this.RARIBLE_API_GET_ASSETS = constants.RARIBLE_API_GET_ASSETS;
            this.OPENSEA_API_GET_ASSETS = constants.OPENSEA_API_GET_ASSETS;
            this.OPENSEA_GET_SINGLE_ASSET = constants.OPENSEA_SINGLE_ASSET;
            this.RARIBLE_GET_SINGLE_ASSET = constants.RARIBLE_SINGLE_ASSET;
            }
            this.collectionRepository = collectionRepository;
    }

    getBorrow = async ( getborrowdto: GetBorrowDto ) =>{
        const borrow = await this.createQueryBuilder("borrow")
                        .leftJoinAndSelect("borrow.owner", "owner")
                        .leftJoinAndSelect("borrow.asset_id", "asset")
                        .where("borrow.id = :id", {id: <number><unknown>getborrowdto.id})
                        .getOne()
        if(!borrow){
            throw new NotFoundException;
        }
        return borrow;
    }

    //function to get single asset from opensea
    async getSingleAssetOpensea( contract_address: string, token_id: string,user:any ){
        try{
            const res = await axios.get(this.OPENSEA_GET_SINGLE_ASSET +contract_address+"/"+token_id + "/",{
                params:{}
            });
            if( user && res.data   ){
                const asset = res.data;
                const asset_data = {
                    asset_id_blockchain: asset.token_id,
                    name: asset.name,
                    description: asset.description || "",
                    image_original_url: asset.image_url,
                    collection: asset.collection.slug,
                    num_sales: asset.num_sales?asset.num_sales:0,
                    animation_original_url: asset.animation_original_url,
                    owner: user.id,
                    platform_name: constants.OPENSEA,
                    price: asset.stats?asset.stats.floor_price:"0",
                    contract_address: asset.asset_contract.address,
                    contract_type: asset.asset_contract.asset_contract
                }
                return asset_data;
            }else{
                return {};
            }
        }catch( error ){
            console.log(error)
            //if rarible get metadata throws error
            return {};
        }
    }

    //function to get single asset from rarible
    async getSingleAssetRarible( contract_address:string, token_id: string,  user:any ){
        try{
            const res = await axios.get(this.RARIBLE_GET_SINGLE_ASSET + "/" + `${contract_address}:${token_id}` + "/" + "meta",{
                params:{}
            });
            if ( user && res.data){
                const asset = res.data;
                const asset_data = {
                    image_original_url : res.data.image.url? res.data.image.url.ORIGINAL:"",
                    name : res.data.name,
                    animation_original_url : res.data.animation?res.data.animation.url.ORIGINAL:"",
                    description : res.data.description,
                    platform_name: constants.RARIBLE,
                    price: asset.creators?asset.creators[0].value:"0",
                    owner: user.id,
                }
                return asset_data;
            }else{
                return {};
            }
        }catch( error ){
            //if rarible get metadata throws error
            return {};
        }
    }


    getBorrows = async ( getborrowsdto: GetBorrowsDto) => {
        const {limit,offset} = getborrowsdto;
        const { sort_key, sort_value } = getborrowsdto;
        delete getborrowsdto.limit 
        delete getborrowsdto.offset
        delete getborrowsdto.sort_key
        delete getborrowsdto.sort_value;
        let sortkey: 'ASC' | 'DESC';
        console.log(this.createQueryBuilder("borrow")
        .leftJoinAndSelect("borrow.owner","owner")
        .leftJoinAndSelect("borrow.asset_id","asset")
        .where("borrow.ownerId = :userId", {userId: <number><unknown>getborrowsdto.id})
        .limit(<number><unknown>limit)
        .offset(<number><unknown>offset)
        .orderBy(sort_key, sortkey)
        .getSql() )
        const borrows = await this.createQueryBuilder("borrow")
                               .leftJoinAndSelect("borrow.owner","owner")
                               .leftJoinAndSelect("borrow.asset_id","asset")
                               .where("borrow.ownerId = :userId", {userId: <number><unknown>getborrowsdto.id})
                               .limit(<number><unknown>limit)
                               .offset(<number><unknown>offset)
                               .orderBy(sort_key, sortkey)
                               .getMany();
        return borrows;
    }

}