import { IsDefined, IsOptional, IsString } from 'class-validator';
import {PrimaryGeneratedColumn, OneToOne ,Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import {Asset} from '../assets/assets.entities';
import {Users} from '../users/users.entities';

//this defines the asset contract
@Entity('assetcontract')
export class AssetContract{
    @PrimaryGeneratedColumn()
    public id: number;

    @OneToOne(() => Asset)
    @JoinColumn()
    public asset_id: Asset;

    @Column()
    public contract_address: string;

    @Column({default: 'nonfungible'})
    public asset_contract_type: string;

    @Column()
    contract_type: string;

    @Column({default: '1'})
    public total_supply: string;

    @Column({default: 'ethereum'})
    @IsString()
    public blockchain: string;  

    @Column()
    @IsOptional()
    public permalink: string;
}

//this will contain the borrow loan details 
//after the asset is applied for borrowig entry is made here
@Entity('borrows')
export class Borrow{
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public amount: string;

    @OneToOne(() => Asset, asset => asset.id)
    public asset_id: Asset;

    @Column()
    public duration: number; //determines the number of days for which loan is meant to be 

    @Column()
    public risk_factor: number;

    @Column({ default: 'pending' })
    @IsOptional()
    public status: string; //status can be pending , approved, rejected, unknown

    @Column()
    @IsOptional()
    public comments?: string; //optional comments

    @Column({ unique:true })
    public approval_transaction_id: string;

    @Column()
    public blockchain: string;

}
