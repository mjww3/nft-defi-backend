import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty, IsNumber, IsOptional, IsObject} from 'class-validator';

export class CreateBorrowDto {

    @IsString()
    @IsDefined()
    @ApiProperty()
    transaction_hash: string ; //the transaction hash of creating a borrow

    @IsString()
    @IsDefined()
    @ApiProperty()
    platform: string;

    @IsString()
    @IsDefined()
    @ApiProperty()
    owner: string;

    @IsString()
    @IsDefined()
    asset_id_blockchain: string;

    @IsString()
    @IsDefined()
    contract_address: string;

    @IsString()
    @ApiPropertyOptional()
    contract_type : string;



}
