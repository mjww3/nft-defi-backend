import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty, IsNumber} from 'class-validator';

export class GetOwnedAssetsDto {
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    //the owner address
    address: string;

    @IsString()
    @ApiProperty()
    @IsDefined()
    limit: string;

    @IsString()
    @IsDefined()
    @ApiProperty()
    offset: string;

    @ApiPropertyOptional()
    platform: string;
}