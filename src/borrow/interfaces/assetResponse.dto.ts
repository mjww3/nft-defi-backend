import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsDefined,
  IsNotEmpty,
  IsNumber,
  IsArray,
  IsOptional,
} from 'class-validator';
import { Assets } from '../../assets/assets.interface';

export class AssetResponseDto {
  @IsArray()
  @ApiProperty()
  assets: Assets[];

  @IsString()
  @ApiProperty()
  @IsOptional()
  continuation_token?: string;
}
