import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsDefined,
  IsNotEmpty,
  IsNumber,
  IsArray,
  IsOptional,
} from 'class-validator';
import { Asset } from '../../assets/assets.entities';

export class CreateAssetContractDto {
  @IsNumber()
  @IsDefined()
  @ApiProperty()
  asset_id: Asset;

  @IsString()
  @IsDefined()
  @ApiProperty()
  contract_address: string;

  @IsString()
  @IsDefined()
  @ApiProperty()
  asset_contract_type: string;

  @IsString()
  @IsDefined()
  @ApiProperty()
  contract_type: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  total_supply: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  permalink: string;
}
