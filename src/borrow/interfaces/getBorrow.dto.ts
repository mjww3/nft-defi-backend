import { ApiProperty } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty, IsNumber, IsOptional} from 'class-validator';

export class GetBorrowDto {
    @IsString()
    @IsDefined()
    @ApiProperty()
    id: string;
}