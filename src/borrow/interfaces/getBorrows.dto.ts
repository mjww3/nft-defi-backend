import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty, IsNumber, IsOptional, isNumber} from 'class-validator';

export class GetBorrowsDto {
    @IsString()
    @IsDefined()
    @ApiProperty()
    id: string;

    @IsString()
    @ApiProperty()
    limit?: string;

    @IsString()
    @ApiProperty()
    offset?: string;

    @ApiPropertyOptional()
    currency: string;

    @ApiPropertyOptional()
    sort_key: string;

    @ApiPropertyOptional()
    sort_value: string; //can be asc or desc

}