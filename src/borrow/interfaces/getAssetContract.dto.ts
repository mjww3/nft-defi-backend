import { ApiProperty } from '@nestjs/swagger';
import {IsString, IsDefined, IsNotEmpty, IsNumber, IsArray, IsOptional} from 'class-validator';

//function to get the asset contracts
export class GetAssetContractDto{
    
    @IsString()
    @IsOptional()
    @ApiProperty()
    address: string; //to get the asset contract of the user

    @IsNumber()
    @IsOptional()
    @ApiProperty()
    asset_id: number; //to get the asset id of the contract
}