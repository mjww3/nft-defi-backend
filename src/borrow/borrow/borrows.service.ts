import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
const Web3 = require('web3');
import axios from 'axios';
import { Connection } from 'typeorm';
import { BorrowRepository } from '../borrow.repository';
import { Assets } from '../../assets/assets.interface';
import constants from './borrow.constants';
import { GetOwnedAssetsDto } from '../interfaces/getOwnedAssets.dto';
import { AssetResponseDto } from '../interfaces/assetResponse.dto';
import { CreateBorrowDto } from '../interfaces/createBorrow.dto';
import { Borrow } from '../borrows.entities';
import { GetBorrowsDto } from '../interfaces/getBorrows.dto';
import { GetBorrowDto } from '../interfaces/getBorrow.dto';
import { Users } from '../../users/users.entities';
import { UserRepository } from '../../users/users.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { TransactionRepository } from '../../transactions/transactions.repository';
import { UtilsService } from '../../utils/utils.service';
const web3 = new Web3(
  process.env.INFURA_API ||
    'https://rinkeby.infura.io/v3/a9e4aa3de809420f8d355b764323069b',
);

// This example provider won't let you make transactions, only read-only calls:
//change the below to test on the testnet
const Redis = require('ioredis');

@Injectable()
export class BorrowService {
  private borrowRepository: BorrowRepository;
  private userRepository: UserRepository;
  private transactionRepository: TransactionRepository;
  public RARIBLE_API_GET_META: string;
  public OPENSEA_GET_SINGLE_ASSET: string;
  public RARIBLE_GET_SINGLE_ASSET: string;
  public RARIBLE_API_GET_ASSETS: string;
  public OPENSEA_API_GET_ASSETS: string;
  constructor(
    private readonly connection: Connection,
    private utilsService: UtilsService,
    @InjectRepository(TransactionRepository)
    transactionRepository: TransactionRepository,
    @InjectRepository(UserRepository) userRepository: UserRepository,
  ) {
    if (process.env.MODE == 'DEV' || !process.env.MODE) {
      this.RARIBLE_API_GET_META = constants.RARIBLE_GET_META_RINKEBY;
      this.RARIBLE_API_GET_ASSETS = constants.RARIBLE_API_GET_ASSETS_RINKEBY;
      this.OPENSEA_API_GET_ASSETS = constants.OPENSEA_API_RINKEBY;
      this.OPENSEA_GET_SINGLE_ASSET = constants.OPENSEA_RINKEBY_SINGLE_ASSET;
      this.RARIBLE_GET_SINGLE_ASSET = constants.RARIBLE_API_GET_ASSETS_RINKEBY;
    } else {
      this.RARIBLE_API_GET_META = constants.RARIBLE_API_GET_META;
      this.RARIBLE_API_GET_ASSETS = constants.RARIBLE_API_GET_ASSETS;
      this.OPENSEA_API_GET_ASSETS = constants.OPENSEA_API_GET_ASSETS;
      this.OPENSEA_GET_SINGLE_ASSET = constants.OPENSEA_SINGLE_ASSET;
      this.RARIBLE_GET_SINGLE_ASSET = constants.RARIBLE_API_GET_ASSETS_RINKEBY;
    }

    this.borrowRepository =
      this.connection.getCustomRepository(BorrowRepository);
    this.userRepository = userRepository;
    this.transactionRepository = transactionRepository;
  }

  //function to get the assets owned by the user
  async getOwnedAssets(
    getOwnedAssetsdto: GetOwnedAssetsDto,
  ): Promise<Assets[]> {
    try {
      const platform = getOwnedAssetsdto.platform;
      let assets;
      if (!platform) {
        assets = await Promise.all([
          this.getAssetsRarible(
            getOwnedAssetsdto.address,
            getOwnedAssetsdto.limit,
            getOwnedAssetsdto.offset,
          ),
          this.getAssetOpensea(
            getOwnedAssetsdto.address,
            getOwnedAssetsdto.limit,
            getOwnedAssetsdto.offset,
          ),
        ]);
      } else if (platform == 'OPENSEA') {
        assets = await Promise.all([
          this.getAssetOpensea(
            getOwnedAssetsdto.address,
            getOwnedAssetsdto.limit,
            getOwnedAssetsdto.offset,
          ),
        ]);
      } else if (platform == 'RARIBLE') {
        assets = await Promise.all([
          this.getAssetsRarible(
            getOwnedAssetsdto.address,
            getOwnedAssetsdto.limit,
            getOwnedAssetsdto.offset,
          ),
        ]);
      } else {
        throw new BadRequestException('Invalid Parameters');
      }
      const finalAssets: Assets[] = [];
      assets.map((asset) => {
        finalAssets.push(...asset.assets);
      });
      //slice the array before returning the response
      return finalAssets.slice(
        parseInt(getOwnedAssetsdto.offset) * parseInt(getOwnedAssetsdto.limit),
        (parseInt(getOwnedAssetsdto.offset) + 1) *
          parseInt(getOwnedAssetsdto.limit),
      );
    } catch (error) {
      return [];
    }
  }

  //function to create the borrow
  async createBorrow(createborrowdto: CreateBorrowDto): Promise<any> {
    const transaction_hash = createborrowdto.transaction_hash;
    if (this.utilsService.validate_txhash(transaction_hash)) {
      const meta = {
        asset_id_blockchain: createborrowdto.asset_id_blockchain,
        contract_address: createborrowdto.contract_address,
        platform: createborrowdto.platform,
        owner: createborrowdto.owner,
        contract_type: createborrowdto.contract_type,
      };
      if (
        createborrowdto.platform != 'OPENSEA' &&
        createborrowdto.platform != 'RARIBLE'
      ) {
        throw new BadRequestException('Invalid platform');
      } else {
        try {
          const transaction =
            await this.transactionRepository.createTransaction({
              tx_hash: transaction_hash.toLowerCase(),
              tx_type: 'CREATE_BORROW_TRANSACTION',
              status: 'PENDING',
              meta: meta,
            });
          return true;
        } catch (error) {
          console.log(error);
          throw new InternalServerErrorException('Could not save borrow');
        }
      }
    } else {
      throw new BadRequestException('Invalid transaction hash');
    }
  }

  //function to get the derived variables from a createborrowdto
  async getDerivedVariables(
    createborrowdto: CreateBorrowDto,
    interest?: number,
    number_of_installments?: number,
  ): Promise<any> {
    const interest_val = interest ? interest : 10;
    const nr_of_installments = number_of_installments
      ? number_of_installments
      : 3;
    return { data: { interest_val, nr_of_installments } };
  }

  //function to get the borrow
  async getBorrow(getborrowdto: GetBorrowDto): Promise<Borrow> {
    return await this.borrowRepository.getBorrow(getborrowdto);
  }

  //function to get the borrows of the address
  async getBorrows(getborrowsdto: GetBorrowsDto): Promise<Borrow[]> {
    return await this.borrowRepository.getBorrows(getborrowsdto);
  }

  //function to get the details of metadata of rarible asset
  async getRaribleTokenMeta(itemId: string) {
    try {
      const res = await axios.get(
        this.RARIBLE_API_GET_META.replace('itemId', itemId),
        {
          params: {},
        },
      );
      const metadata: any = {};
      if (res.data) {
        metadata.image_original_url = res.data.image.url
          ? res.data.image.url.ORIGINAL
          : '';
        metadata.name = res.data.name;
        metadata.animation_original_url = res.data.animation
          ? res.data.animation.url.ORIGINAL
          : '';
        metadata.description = res.data.description;

        return metadata;
      } else {
        return metadata;
      }
    } catch (error) {
      //if rarible get metadata throws error
      return {};
    }
  }

  //get assets from rarible
  async getAssetsRarible(
    address: string,
    limit: string,
    page: string,
    continuation_token?: string,
  ): Promise<AssetResponseDto> {
    try {
      const user = await this.userRepository.getUser({
        address: address.toLowerCase(),
      });
      const res = await axios.get(this.RARIBLE_API_GET_ASSETS, {
        params: {
          owner: user.address,
          size: limit,
          continuation: continuation_token,
        },
      });
      let count = 0;
      const assets: Assets[] = [];
      if (res.data && res.data['items'].length) {
        for (let i = 0; i < res.data['items'].length; i++) {
          const asset = res.data['items'][i];
          let metadata;
          if (asset.id) {
            metadata = await this.getRaribleTokenMeta(asset.id);
          }
          assets.push({
            id: asset.id,
            asset_id_blockchain: asset.tokenId,
            num_sales: asset.royalties ? asset.royalties.length : 0,
            owner: user,
            platform_name: constants.RARIBLE,
            price: asset.creators ? asset.creators[0].value : '0',
            ...metadata,
          });
          count++;
          if (count == res.data['items'].length) {
            return { assets, continuation_token };
          }
        }
      } else {
        return { assets: [], continuation_token: null };
      }
    } catch (error) {
      return { assets: [], continuation_token: null };
    }
  }

  //get assets from opensea
  async getAssetOpensea(
    address: string,
    limit: string,
    offset: string,
  ): Promise<AssetResponseDto> {
    try {
      const user = await this.userRepository.getUser({
        address: address.toLowerCase(),
      });
      const res = await axios.get(this.OPENSEA_API_GET_ASSETS, {
        params: {
          owner: user.address,
          limit: limit,
          offset: parseInt(offset),
        },
      });
      const assets: Assets[] = [];
      if (res.data && res.data['assets'].length) {
        res.data['assets'].map((asset) => {
          assets.push({
            id: asset.id,
            asset_id_blockchain: asset.token_id,
            name: asset.name,
            description: asset.description || '',
            image_original_url: asset.image_url,
            collection: asset.collection.slug,
            num_sales: asset.num_sales ? asset.num_sales : 0,
            animation_original_url: asset.animation_original_url,
            owner: user,
            platform_name: constants.OPENSEA,
            price: asset.last_sale
              ? this.getPrice(asset.last_sale.total_price).toString()
              : '0',
            currency: asset.last_sale
              ? this.getCurrency(asset.last_sale.payment_token.symbol)
              : 'ETHEREUM',
            contract_address: asset.asset_contract.address,
            contract_type: asset.asset_contract.schema_name,
          });
        });
        return { assets: assets };
      } else {
        return { assets: [] };
      }
    } catch (error) {
      console.log(error);
      return { assets: [] };
    }
  }

  getPrice(value) {
    return value / Math.pow(10, 18);
  }

  getCurrency(currency) {
    if (currency == 'ETH') {
      return 'ETHEREUM';
    } else {
      return 'ETHEREUM';
    }
  }
}
