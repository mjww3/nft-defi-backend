import { Body, Controller, Get, Post, Put, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { GetOwnedAssetsDto } from '../interfaces/getOwnedAssets.dto';
import {BorrowService} from './borrows.service'
import {Assets} from '../../assets/assets.interface';
import { Borrow } from '../borrows.entities';
import { CreateBorrowDto } from '../interfaces/createBorrow.dto';
import { GetBorrowDto } from '../interfaces/getBorrow.dto';
import { GetBorrowsDto } from '../interfaces/getBorrows.dto';
import { ApiBadGatewayResponse, ApiBadRequestResponse, ApiTags } from '@nestjs/swagger';

@ApiTags('Borrows')
@Controller()
export class BorrowController {
    constructor(private readonly borrowService: BorrowService){}

    @ApiBadRequestResponse({status:400})
    @ApiBadGatewayResponse({status: 401})
    @Get("/ownedAssets")
    @UsePipes(new ValidationPipe())
    async getOwnedAssets(@Query() getOwnedAssetsdto: GetOwnedAssetsDto ) : Promise<any>{
        const data =  await this.borrowService.getOwnedAssets( getOwnedAssetsdto );
        return { data: data }
    }

    @ApiBadRequestResponse({status:400})
    @ApiBadGatewayResponse({status: 401})
    @Post('/createBorrow')
    @UsePipes( new ValidationPipe())
    async createBorrow( @Body() createborrowdto: CreateBorrowDto ) : Promise<any>{
        const borrow  = await this.borrowService.createBorrow( createborrowdto );
        return {data: borrow }
    }

    @ApiBadRequestResponse({status:400})
    @ApiBadGatewayResponse({status: 401})
    @Get('/borrow')
    @UsePipes( new ValidationPipe())
    async getBorrow( @Query() getborrowdto: GetBorrowDto ) : Promise<any>{
        const borrow  = await this.borrowService.getBorrow( getborrowdto );
        return { data: borrow }
    }

    @ApiBadRequestResponse({status:400})
    @ApiBadGatewayResponse({status: 401})
    @Get('/borrows')
    @UsePipes( new ValidationPipe())
    async  getBorrows( @Query() getborrowsto: GetBorrowsDto ) : Promise<any>{
        const borrows = await this.borrowService.getBorrows( getborrowsto );
        return {data: borrows}
    }

    @ApiBadRequestResponse({status:400})
    @ApiBadGatewayResponse({status: 401})
    @Get('/getDerivedVariables')
    @UsePipes( new ValidationPipe())
    getDerivedVariables( @Query() createborrowdto: CreateBorrowDto ) :Promise<any>{
        return this.borrowService.getDerivedVariables(createborrowdto);
    }
}
