const constants = {
    OPENSEA: "OPENSEA",
    RARIBLE: "RARIBLE",
    OPENSEA_API_GET_ASSETS:"https://api.opensea.io/api/v1/assets",
    RARIBLE_API_GET_ASSETS:"https://api.rarible.com/protocol/v0.1/ethereum/nft/items/byOwner",
    RARIBLE_API_GET_META: "https://api.rarible.com/protocol/v0.1/ethereum/nft/items/itemId/meta",
    RARIBLE_API_GET_ASSETS_RINKEBY: "https://api-staging.rarible.com/protocol/v0.1/ethereum/nft/items/byOwner",
    OPENSEA_API_RINKEBY:"https://rinkeby-api.opensea.io/api/v1/assets",
    RARIBLE_GET_META_RINKEBY: "https://api-staging.rarible.com/protocol/v0.1/ethereum/nft/items/itemId/meta",
    OPENSEA_RINKEBY_SINGLE_ASSET: "https://rinkeby-api.opensea.io/api/v1/asset/",
    OPENSEA_SINGLE_ASSET: "https://api.opensea.io/api/v1/asset/",
    RARIBLE_SINGLE_ASSET_RINKEBY: "http://api-staging.rarible.com/protocol/v0.1/ethereum/nft/items/",
    RARIBLE_SINGLE_ASSET: "http://api.rarible.com/protocol/v0.1/ethereum/nft/items/"
    //add constants of derived variables here
}
export default constants;