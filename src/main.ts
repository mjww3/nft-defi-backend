import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { AssetsModule } from './assets/assets.module';
import { BorrowModule } from './borrow/borrow.module';
import { CollectionsModule } from './collections/collections.module';
import { LendModule } from './lend/lend.module';
import { TransactionsModule } from './transactions/transactions.module';
import { UsersModule } from './users/users.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { logger: true });

  const config = new DocumentBuilder()
    .setTitle('NFT defi')
    .setDescription('The nft defi module')
    .setVersion('1.0')
    .addTag('defi')
    .build();
  const document = SwaggerModule.createDocument(app, config, {
    include: [
      UsersModule,
      AssetsModule,
      BorrowModule,
      LendModule,
      CollectionsModule,
      TransactionsModule,
    ],
  });
  SwaggerModule.setup('api', app, document);
  app.enableCors();
  await app.listen(3001);
}
bootstrap();
