import {IsString, IsNumberString, IsDefined, IsObject, IsBoolean, IsOptional, IsNumber, IsNotEmpty} from 'class-validator';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

export class UpdateTransactionsDto{
    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    id: number;

    @ApiProperty()
    @IsString()
    @IsNotEmpty()
    status: string;
}