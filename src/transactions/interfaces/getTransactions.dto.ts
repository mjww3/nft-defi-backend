import {IsString, IsNumberString, IsDefined, IsObject, IsBoolean, IsOptional, IsNumber} from 'class-validator';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

export class GetTransactionsDto{
    @ApiPropertyOptional()
    user: number;

    @ApiPropertyOptional()
    asset: number;

    @ApiPropertyOptional()
    status: string;

    @ApiPropertyOptional()
    tx_type: string;

    @ApiPropertyOptional()
    limit?: string;

    @ApiPropertyOptional()
    offset?: string;

    @ApiPropertyOptional()
    sort_key: string;

    @ApiPropertyOptional()
    sort_value: string;
}