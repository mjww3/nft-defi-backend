import {IsString, IsNumberString, IsDefined, IsObject, IsBoolean, IsOptional, IsNumber} from 'class-validator';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

export class CreateTransactionDto {
    @IsString()
    @IsDefined()
    @ApiProperty()
    tx_hash: string;

    @IsString()
    @IsDefined()
    @ApiProperty()
    tx_type: string;

    @IsString()
    @IsDefined()
    @ApiProperty()
    status: string;

    @IsObject()
    @IsDefined()
    @ApiProperty()
    meta: object;

}