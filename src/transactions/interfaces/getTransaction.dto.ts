import {IsString, IsNumberString, IsDefined, IsObject, IsBoolean, IsOptional, IsNumber} from 'class-validator';
import { ApiProperty,ApiPropertyOptional } from '@nestjs/swagger';

export class GetTransactionDto{
    @ApiProperty()
    @IsString()
    @IsDefined()
    tx_hash: string;
}