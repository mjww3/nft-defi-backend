import {PrimaryGeneratedColumn, Column, Entity, ManyToOne, JoinColumn } from 'typeorm';
import { Asset } from '../assets/assets.entities';

@Entity('transactions')
class Transaction{
    @PrimaryGeneratedColumn()
    public id: number;

    @Column({unique: true})
    public tx_hash: string;

    @Column()
    public tx_type: string;

    @ManyToOne(() => Asset, asset => asset.transactions)
    @JoinColumn()
    public asset: Asset; //the assetid of the asset for which transaction is being done

    @Column()
    public status: string;

    @Column({type: 'jsonb',nullable: true})
    public meta: object;
}

export default Transaction;