import Transaction from './transactions.entities';
import { EntityRepository, Repository } from 'typeorm';
import { GetTransactionDto } from './interfaces/getTransaction.dto';
import { GetTransactionsDto } from './interfaces/getTransactions.dto';
import { CreateTransactionDto } from './interfaces/createTransaction.dto';

//assets
@EntityRepository(Transaction)
export class TransactionRepository extends Repository<Transaction> {
  //get the transaction
  getTransaction = async (gettransactionDto: GetTransactionDto) => {
    const transaction = await this.findOne(gettransactionDto);
    return transaction;
  };

  //create transaction
  //transaction will be created by inner api calls within the system
  createTransaction = async (createtransactiondto: CreateTransactionDto) => {
    const transaction = this.create(createtransactiondto);
    await this.save(transaction);
    return transaction;
  };

  //get the transactions
  getTransactions = async (gettransactionsDto: GetTransactionsDto) => {
    const { limit, offset } = gettransactionsDto;

    const { sort_key, sort_value } = gettransactionsDto;
    delete gettransactionsDto.limit;
    delete gettransactionsDto.offset;
    delete gettransactionsDto.sort_key;
    delete gettransactionsDto.sort_value;
    const obj: any = { where: gettransactionsDto };
    if (limit) {
      obj.take = limit;
    }
    if (offset) {
      obj.skip = offset;
    }
    if (sort_key) {
      obj.order = { sort_key: 'ASC' };
    }
    if (sort_value) {
      if (sort_key == 'asc') {
        obj.order.sort_key = 'ASC';
      } else {
        obj.order.sort_key = 'DESC';
      }
    }
    const assets = await this.find(obj);
    return assets;
  };

  //update the status of the transaction
  updateTransaction = async (id: string, status: string) => {
    await this.update(id, { status: status }).then(
      (response) => response.raw[0],
    );
    const asset = await this.findOne(id);
    return asset;
  };
}
