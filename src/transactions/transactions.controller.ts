import { Body, Controller, Get, Post, Query, UsePipes, ValidationPipe } from '@nestjs/common';
import { GetTransactionDto } from './interfaces/getTransaction.dto';
import { TransactionsService } from './transactions.service';
import { ApiBadGatewayResponse, ApiResponse, ApiTags } from "@nestjs/swagger";
import { GetTransactionsDto } from './interfaces/getTransactions.dto';
import { CreateTransactionDto } from './interfaces/createTransaction.dto';

@ApiTags("Transactions")
@Controller()
export class TransactionsController {
    constructor(private readonly transactionService: TransactionsService){}

    @ApiResponse({ status: 200 })
    @ApiBadGatewayResponse({status:400})
    @ApiBadGatewayResponse({status: 401})
    @Post('/transaction')
    @UsePipes(new ValidationPipe())
    async createTransaction( @Body() createtransctionDto: CreateTransactionDto ){
        const transaction = await this.transactionService.createTransaction(createtransctionDto );
        return { data: transaction }
    }


    //get an asset
    @ApiResponse({ status: 200 })
    @Get('/transaction')
    @UsePipes(new ValidationPipe())
    async getTransaction( @Query() gettransactionDto: GetTransactionDto ){
        const transaction = await this.transactionService.getTransaction(gettransactionDto );
        return { data: transaction }
    }

    //get multiple assets
    //supports filter query too
    @ApiResponse({ status: 200 })
    @Get('/transactions')
    @UsePipes(new ValidationPipe())
    async getTransactions( @Query() gettransactionsDto: GetTransactionsDto ){
        const transactions = await this.transactionService.getTransactions(gettransactionsDto);
        return { data: transactions }
    }
    
}
