import { Injectable } from '@nestjs/common';
import {Connection} from 'typeorm'
import Transactions from './transactions.entities';
import { TransactionRepository } from './transactions.repository';
import { GetTransactionDto } from './interfaces/getTransaction.dto';
import { UpdateTransactionsDto } from './interfaces/updateTransactions.dto';
import { GetTransactionsDto } from './interfaces/getTransactions.dto';
import { CreateTransactionDto } from './interfaces/createTransaction.dto';

@Injectable()
export class TransactionsService {
    //get asset
    private transactionRepository: TransactionRepository;
    constructor( private readonly connection: Connection ){
        this.transactionRepository = this.connection.getCustomRepository(TransactionRepository);
    }

    async createTransaction( createtransactiondto: CreateTransactionDto) : Promise<Transactions>{
        return await this.transactionRepository.createTransaction( createtransactiondto );
    }

    async getTransaction( getTransactionDto: GetTransactionDto ) : Promise<Transactions>{
        return await this.transactionRepository.getTransaction( getTransactionDto );
    }

    async getTransactions( getTransactionsDto: GetTransactionsDto ) :Promise<Transactions[]>{
        return await this.transactionRepository.getTransactions( getTransactionsDto );
    }

    async updateTransaction(id:string, status: string ) :Promise<Transactions>{
        return await this.transactionRepository.updateTransaction(id , status);
    }
}
