import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BorrowRepository } from '../borrow/borrow.repository';
import { Borrow } from '../borrow/borrows.entities';
import { TransactionsController } from './transactions.controller';
import Transaction from './transactions.entities';
import { TransactionRepository } from './transactions.repository';
import { TransactionsService } from './transactions.service';

@Module({
  imports: [TypeOrmModule.forFeature([Transaction, TransactionRepository,Borrow,BorrowRepository])],
  providers: [TransactionsService],
  controllers: [TransactionsController]
})
export class TransactionsModule {}
